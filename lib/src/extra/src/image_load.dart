import 'dart:typed_data';

import 'package:ap_ui_kit/ap_ui_kit.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class APImageLoad extends StatelessWidget {
  const APImageLoad({
    this.path,
    this.image,
    this.width,
    this.height,
    this.fit = BoxFit.contain,
    this.imageLoadType = ImageLoadType.network,
    this.blurhash,
    super.key,
    this.radius,
    this.isCircle = false,
  });

  final String? path;
  final Uint8List? image;
  final double? width;
  final double? height;
  final BoxFit fit;
  final String? blurhash;
  final ImageLoadType imageLoadType;
  final double? radius;
  final bool isCircle;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final badgeBadColors = theme.extension<BadgeBadColors>()!;

    final Widget imageWidget;

    switch (imageLoadType) {
      case ImageLoadType.asset:
        imageWidget = Image.asset(
          path ?? '',
          width: width,
          height: height,
          fit: fit,
        );
      case ImageLoadType.file:
        imageWidget = Image.asset(
          path ?? '',
          width: width,
          height: height,
          fit: fit,
        );
      case ImageLoadType.network:
        final String? sessionToken = APSessionInheritedWidget.of(context)?.sessionToken;
        final httpHeaders = <String, String>{};

        if (sessionToken != null) {
          httpHeaders.addAll({'Authorization': 'Bearer $sessionToken'});
        }

        imageWidget = CachedNetworkImage(
          httpHeaders: httpHeaders,
          imageUrl: path ?? '',
          width: width,
          height: height,
          fit: fit,
          imageBuilder: (context, imageProvider) => DecoratedBox(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,
                //colorFilter: ColorFilter.mode(Colors.red, BlendMode.colorBurn),
              ),
            ),
          ),
          errorWidget: (
            BuildContext context,
            String url,
            Object error,
          ) {
            return SizedBox(
              width: width,
              height: height,
              child: DecoratedBox(
                decoration: BoxDecoration(
                  color: badgeBadColors.filledFill,
                ),
                child: Center(
                  child: APIconWidget.size2XM(
                    color: badgeBadColors.strokedFill,
                    type: APIconType.picture,
                  ),
                ),
              ),
            );
          },
        );

      case ImageLoadType.memory:
        imageWidget = Image.memory(
          image ?? Uint8List(0),
          width: width,
          height: height,
          fit: fit,
        );
    }

    if (radius != null) {
      return ClipRRect(
        borderRadius: isCircle
            ? BorderRadius.circular(
                radius!,
              )
            : BorderRadius.all(
                Radius.circular(
                  radius!,
                ),
              ),
        child: imageWidget,
      );
    }

    return SizedBox(child: imageWidget);
  }
}
