import 'package:ap_ui_kit/ap_ui_kit.dart';
import 'package:ap_ui_kit/generated/assets.dart';

class FileExtensions {
  static String? getFileTypeAsset(String fileType) {
    return getAssetImagePath(getFileTypeString(fileType));
  }

  static String? getAssetImagePath(APChatFileType type) {
    switch (type) {
      case APChatFileType.csv:
        return Assets.fileTypeCsv.packagePrefix;
      case APChatFileType.doc:
        return Assets.fileTypeDoc.packagePrefix;
      case APChatFileType.docx:
        return Assets.fileTypeDocx.packagePrefix;
      case APChatFileType.exl:
        return Assets.fileTypeExl.packagePrefix;
      case APChatFileType.mp3:
        return Assets.fileTypeMp3.packagePrefix;
      case APChatFileType.pdf:
        return Assets.fileTypePdf.packagePrefix;
      case APChatFileType.ppt:
        return Assets.fileTypePpt.packagePrefix;
      case APChatFileType.pptx:
        return Assets.fileTypePptx.packagePrefix;
      case APChatFileType.rar:
        return Assets.fileTypeRar.packagePrefix;
      case APChatFileType.xls:
        return Assets.fileTypeXls.packagePrefix;
      case APChatFileType.xlsx:
        return Assets.fileTypeXlsx.packagePrefix;
      case APChatFileType.zip:
        return Assets.fileTypeZip.packagePrefix;
      case APChatFileType.txt:
        return Assets.fileTypeTxt.packagePrefix;
      default:
        return Assets.fileTypeUnknown.packagePrefix;
    }
  }

  static String? convertFileNameToImagePath(String filename) {
    return getAssetImagePath(convertFileNameToFileType(filename));
  }

  static APChatFileType convertFileNameToFileType(String filename) {
    final List<String> substrings = filename.split('.');
    final String fileType;

    if (substrings.isNotEmpty) {
      fileType = substrings.last;
    } else {
      fileType = '';
    }
    return getFileTypeString(fileType);
  }

  static APChatFileType getFileTypeString(String fileType) {
    switch (fileType.toLowerCase()) {
      case 'png':
        return APChatFileType.png;
      case 'jpg':
        return APChatFileType.jpg;
      case 'jpeg':
        return APChatFileType.jpeg;
      case 'mp4':
        return APChatFileType.mp4;
      case 'csv':
        return APChatFileType.csv;
      case 'doc':
        return APChatFileType.doc;
      case 'docx':
        return APChatFileType.docx;
      case 'exl':
        return APChatFileType.exl;
      case 'mp3':
        return APChatFileType.mp3;
      case 'pdf':
        return APChatFileType.pdf;
      case 'ppt':
        return APChatFileType.ppt;
      case 'pptx':
        return APChatFileType.pptx;
      case 'rar':
        return APChatFileType.rar;
      case 'xls':
        return APChatFileType.xls;
      case 'xlsx':
        return APChatFileType.xlsx;
      case 'zip':
        return APChatFileType.zip;
      case 'txt':
        return APChatFileType.txt;
      default:
        return APChatFileType.unknown;
    }
  }

  static APChatMediaFileType getFileMediaType(String extension) {
    switch (getFileTypeString(extension)) {
      case APChatFileType.png:
      case APChatFileType.jpg:
      case APChatFileType.jpeg:
        return APChatMediaFileType.image;
      case APChatFileType.mp4:
        return APChatMediaFileType.video;
      default:
        return APChatMediaFileType.file;
    }
  }
}
