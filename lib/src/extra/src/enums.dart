enum ImageLoadType {
  asset,
  file,
  network,
  memory,
}

enum APChatMediaFileType {
  video,
  image,
  file,
}

enum APChatFileType {
  png,
  jpg,
  jpeg,
  mp4,
  doc,
  docx,
  exl,
  csv,
  xls,
  xlsx,
  pdf,
  mp3,
  txt,
  ppt,
  pptx,
  zip,
  rar,
  unknown,
}

enum APFileChatModeType {
  none,
  upload,
  download,
}

enum APFileChatStateType {
  regular,
  progress,
  error,
}
