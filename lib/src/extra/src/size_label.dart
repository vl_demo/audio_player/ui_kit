import 'dart:math';

import 'package:intl/intl.dart';

class SizeLabel {
  static const String byte = 'байт';
  static const String kilobyte = 'КБ';
  static const String megabyte = 'МБ';
  static const String gigabyte = 'ГБ';
  static const String terabyte = 'ТБ';

  static String convert(int size) {
    var f = NumberFormat("###.##", "en_US");

    // byte
    if (size < 1024) {
      return f.format(size);
    }

    // kilobyte
    if (size < pow(1024, 2)) {
      return f.format(size / 1024);
    }

    // megabyte
    if (size < pow(1024, 3)) {
      return f.format(size / pow(1024, 2));
    }

    // gigabyte
    if (size < pow(1024, 4)) {
      return f.format(size / pow(1024, 3));
    }

    // terabyte
    if (size < pow(1024, 5)) {
      return f.format(size / pow(1024, 4));
    }

    return '';
  }

  static String convertLabel(int size) {
    // byte
    if (size < 1024) {
      return SizeLabel.byte;
    }

    // kilobyte
    if (size < pow(1024, 2)) {
      return SizeLabel.kilobyte;
    }

    // megabyte
    if (size < pow(1024, 3)) {
      return SizeLabel.megabyte;
    }

    // gigabyte
    if (size < pow(1024, 4)) {
      return SizeLabel.gigabyte;
    }

    // terabyte
    if (size < pow(1024, 5)) {
      return SizeLabel.terabyte;
    }

    return '';
  }

  static String convertFullLabel(int size) => '${convert(size)} ${convertLabel(size)}';
}
