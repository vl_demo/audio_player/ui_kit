import 'package:flutter/widgets.dart';

class APSessionInheritedWidget extends InheritedWidget {
  final String? sessionToken;

  const APSessionInheritedWidget({
    required super.child,
    this.sessionToken,
    super.key,
  });

  static APSessionInheritedWidget? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<APSessionInheritedWidget>();
  }

  @override
  bool updateShouldNotify(APSessionInheritedWidget oldWidget) => sessionToken != oldWidget.sessionToken;
}
