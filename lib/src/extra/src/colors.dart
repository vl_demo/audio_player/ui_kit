import 'dart:ui';

class APColorsUtil {
  static Color fromUuid(String uuid) {
    final buffer = StringBuffer()..write('f0${uuid[0]}0${uuid[1]}0${uuid[2]}0');

    return Color(int.parse(buffer.toString(), radix: 16));
  }
}
