part of '../../index.dart';

class SvgSizesIcon {
  const SvgSizesIcon({
    pathSizeL,
    pathSizeM,
    pathSizeS,
    pathSizeXS,
    pathSize2XS,
  })  : _sizeL = pathSizeL,
        _sizeM = pathSizeM,
        _sizeS = pathSizeS,
        _sizeXS = pathSizeXS,
        _size2XS = pathSize2XS;

  final String _sizeL;
  final String _sizeM;
  final String _sizeS;
  final String _sizeXS;
  final String _size2XS;

  String get sizeL => _getSvgPicture(_sizeL);
  String get sizeM => _getSvgPicture(_sizeM);
  String get sizeS => _getSvgPicture(_sizeS);
  String get sizeXS => _getSvgPicture(_sizeXS);
  String get size2XS => _getSvgPicture(_size2XS);

  String _getSvgPicture(String path) => path.packagePrefix;
}

class APSvgIconAssets {
  static const defaultCheck = SvgSizesIcon(
    pathSizeL: Assets.checkDefaultSizeL,
    pathSizeM: Assets.checkDefaultSizeM,
    pathSizeS: Assets.checkDefaultSizeS,
    pathSizeXS: Assets.checkDefaultSizeXS,
    pathSize2XS: Assets.checkDefaultSize2XS,
  );

  static const eyeConturClose = SvgSizesIcon(
    pathSizeL: Assets.eyeConturCloseSizeL,
    pathSizeM: Assets.eyeConturCloseSizeM,
    pathSizeS: Assets.eyeConturCloseSizeS,
    pathSizeXS: Assets.eyeConturCloseSizeXS,
    pathSize2XS: Assets.eyeConturCloseSizeXS,
  );

  static const eyeConturOpen = SvgSizesIcon(
    pathSizeL: Assets.eyeConturOpenSizeL,
    pathSizeM: Assets.eyeConturOpenSizeM,
    pathSizeS: Assets.eyeConturOpenSizeS,
    pathSizeXS: Assets.eyeConturOpenSizeXS,
    pathSize2XS: Assets.eyeConturOpenSizeXS,
  );

  static const backspace = SvgSizesIcon(
    pathSizeL: Assets.backspaceSizeL,
    pathSizeM: Assets.backspaceSizeM,
    pathSizeS: Assets.backspaceSizeS,
    pathSizeXS: Assets.backspaceSizeXS,
    pathSize2XS: Assets.backspaceSizeXS,
  );

  static const block = SvgSizesIcon(
    pathSizeL: Assets.blockSizeL,
    pathSizeM: Assets.blockSizeM,
    pathSizeS: Assets.blockSizeS,
    pathSizeXS: Assets.blockSizeXS,
    pathSize2XS: Assets.blockSizeXS,
  );

  static const doorOut = SvgSizesIcon(
    pathSizeL: Assets.doorOutSizeL,
    pathSizeM: Assets.doorOutSizeM,
    pathSizeS: Assets.doorOutSizeS,
    pathSizeXS: Assets.doorOutSizeXS,
    pathSize2XS: Assets.doorOutSizeXS,
  );

  static const doorIn = SvgSizesIcon(
    pathSizeL: Assets.doorInSizeL,
    pathSizeM: Assets.doorInSizeM,
    pathSizeS: Assets.doorInSizeS,
    pathSizeXS: Assets.doorInSizeXS,
    pathSize2XS: Assets.doorInSizeXS,
  );

  static const key = SvgSizesIcon(
    pathSizeL: Assets.keySizeL,
    pathSizeM: Assets.keySizeM,
    pathSizeS: Assets.keySizeS,
    pathSizeXS: Assets.keySizeXS,
    pathSize2XS: Assets.keySizeXS,
  );

  static const lock = SvgSizesIcon(
    pathSizeL: Assets.lockSizeL,
    pathSizeM: Assets.lockSizeL,
    pathSizeS: Assets.lockSizeL,
    pathSizeXS: Assets.lockSizeL,
    pathSize2XS: Assets.lockSizeL,
  );

  static const pinLock = SvgSizesIcon(
    pathSizeL: Assets.pinLockSizeL,
    pathSizeM: Assets.pinLockSizeM,
    pathSizeS: Assets.pinLockSizeS,
    pathSizeXS: Assets.pinLockSizeXS,
    pathSize2XS: Assets.pinLockSizeXS,
  );

  static const userGroup = SvgSizesIcon(
    pathSizeL: Assets.userGroupSizeL,
    pathSizeM: Assets.userGroupSizeM,
    pathSizeS: Assets.userGroupSizeS,
    pathSizeXS: Assets.userGroupSizeXS,
    pathSize2XS: Assets.userGroupSize2XS,
  );

  static const cancelDefault = SvgSizesIcon(
    pathSizeL: Assets.cancelDefaultSizeL,
    pathSizeM: Assets.cancelDefaultSizeM,
    pathSizeS: Assets.cancelDefaultSizeS,
    pathSizeXS: Assets.cancelDefaultSizeXS,
    pathSize2XS: Assets.cancelDefaultSize2XS,
  );

  static const emojiGood = SvgSizesIcon(
    pathSizeL: Assets.emojiGoodSizeL,
    pathSizeM: Assets.emojiGoodSizeM,
    pathSizeS: Assets.emojiGoodSizeS,
    pathSizeXS: Assets.emojiGoodSizeXS,
    pathSize2XS: Assets.emojiGoodSizeXS,
  );

  static const clip = SvgSizesIcon(
    pathSizeL: Assets.clipSizeL,
    pathSizeM: Assets.clipSizeM,
    pathSizeS: Assets.clipSizeS,
    pathSizeXS: Assets.clipSizeXS,
    pathSize2XS: Assets.clipSizeXS,
  );

  static const mention = SvgSizesIcon(
    pathSizeL: Assets.mentionSizeL,
    pathSizeM: Assets.mentionSizeM,
    pathSizeS: Assets.mentionSizeS,
    pathSizeXS: Assets.mentionSizeXS,
    pathSize2XS: Assets.mentionSizeXS,
  );

  static const groupChat = SvgSizesIcon(
    pathSizeL: Assets.groupChatSizeL,
    pathSizeM: Assets.groupChatSizeM,
    pathSizeS: Assets.groupChatSizeS,
    pathSizeXS: Assets.groupChatSizeXS,
    pathSize2XS: Assets.groupChatSize2XS,
  );

  static const bold = SvgSizesIcon(
    pathSizeL: Assets.boldSizeL,
    pathSizeM: Assets.boldSizeM,
    pathSizeS: Assets.boldSizeS,
    pathSizeXS: Assets.boldSizeXS,
    pathSize2XS: Assets.boldSizeXS,
  );

  static const channel = SvgSizesIcon(
    pathSizeL: Assets.channelSizeL,
    pathSizeM: Assets.channelSizeM,
    pathSizeS: Assets.channelSizeS,
    pathSizeXS: Assets.channelSizeXS,
    pathSize2XS: Assets.channelSizeXS,
  );

  static const underline = SvgSizesIcon(
    pathSizeL: Assets.underlineSizeL,
    pathSizeM: Assets.underlineSizeM,
    pathSizeS: Assets.underlineSizeS,
    pathSizeXS: Assets.underlineSizeXS,
    pathSize2XS: Assets.underlineSizeXS,
  );

  static const italic = SvgSizesIcon(
    pathSizeL: Assets.italicSizeL,
    pathSizeM: Assets.italicSizeM,
    pathSizeS: Assets.italicSizeS,
    pathSizeXS: Assets.italicSizeXS,
    pathSize2XS: Assets.italicSizeXS,
  );

  static const pinYes = SvgSizesIcon(
    pathSizeL: Assets.pinYesSizeL,
    pathSizeM: Assets.pinYesSizeM,
    pathSizeS: Assets.pinYesSizeS,
    pathSizeXS: Assets.pinYesSizeXS,
    pathSize2XS: Assets.pinYesSizeXS,
  );

  static const pinNo = SvgSizesIcon(
    pathSizeL: Assets.pinNoSizeL,
    pathSizeM: Assets.pinNoSizeM,
    pathSizeS: Assets.pinNoSizeS,
    pathSizeXS: Assets.pinNoSizeXS,
    pathSize2XS: Assets.pinNoSizeXS,
  );

  static const addEmoji = SvgSizesIcon(
    pathSizeL: Assets.addEmojiSizeL,
    pathSizeM: Assets.addEmojiSizeM,
    pathSizeS: Assets.addEmojiSizeS,
    pathSizeXS: Assets.addEmojiSizeS,
    pathSize2XS: Assets.addEmojiSizeXS,
  );

  static const download = SvgSizesIcon(
    pathSizeL: Assets.downloadSizeL,
    pathSizeM: Assets.downloadSizeM,
    pathSizeS: Assets.downloadSizeS,
    pathSizeXS: Assets.downloadSizeS,
    pathSize2XS: Assets.downloadSizeXS,
  );

  static const chevronLeft = SvgSizesIcon(
    pathSizeL: Assets.chevronLeftSizeL,
    pathSizeM: Assets.chevronLeftSizeM,
    pathSizeS: Assets.chevronLeftSizeS,
    pathSizeXS: Assets.chevronLeftSizeS,
    pathSize2XS: Assets.chevronLeftSizeXS,
  );

  static const chevronRight = SvgSizesIcon(
    pathSizeL: Assets.chevronRightSizeL,
    pathSizeM: Assets.chevronRightSizeM,
    pathSizeS: Assets.chevronRightSizeS,
    pathSizeXS: Assets.chevronRightSizeS,
    pathSize2XS: Assets.chevronRightSizeXS,
  );

  static const chevronUp = SvgSizesIcon(
    pathSizeL: Assets.chevronUpSizeL,
    pathSizeM: Assets.chevronUpSizeM,
    pathSizeS: Assets.chevronUpSizeS,
    pathSizeXS: Assets.chevronUpSizeS,
    pathSize2XS: Assets.chevronUpSizeXS,
  );

  static const chevronDown = SvgSizesIcon(
    pathSizeL: Assets.chevronDownSizeL,
    pathSizeM: Assets.chevronDownSizeM,
    pathSizeS: Assets.chevronDownSizeS,
    pathSizeXS: Assets.chevronDownSizeS,
    pathSize2XS: Assets.chevronDownSizeXS,
  );

  static const bookmarkOutline = SvgSizesIcon(
    pathSizeL: Assets.bookmarkOutlineSizeL,
    pathSizeM: Assets.bookmarkOutlineSizeM,
    pathSizeS: Assets.bookmarkOutlineSizeS,
    pathSizeXS: Assets.bookmarkOutlineSizeS,
    pathSize2XS: Assets.bookmarkOutlineSizeXS,
  );

  static const bookmarkFilled = SvgSizesIcon(
    pathSizeL: Assets.bookmarkFilledSizeL,
    pathSizeM: Assets.bookmarkFilledSizeM,
    pathSizeS: Assets.bookmarkFilledSizeS,
    pathSizeXS: Assets.bookmarkFilledSizeS,
    pathSize2XS: Assets.bookmarkFilledSizeXS,
  );

  static const penEdit = SvgSizesIcon(
    pathSizeL: Assets.penEditSizeL,
    pathSizeM: Assets.penEditSizeM,
    pathSizeS: Assets.penEditSizeS,
    pathSizeXS: Assets.penEditSizeXS,
    pathSize2XS: Assets.penEditSizeXS,
  );

  static const message = SvgSizesIcon(
    pathSizeL: Assets.messageSizeL,
    pathSizeM: Assets.messageSizeM,
    pathSizeS: Assets.messageSizeS,
    pathSizeXS: Assets.messageSizeXS,
    pathSize2XS: Assets.messageSizeXS,
  );

  static const play = SvgSizesIcon(
    pathSizeL: Assets.playSizeL,
    pathSizeM: Assets.playSizeM,
    pathSizeS: Assets.playSizeS,
    pathSizeXS: Assets.playSizeXS,
    pathSize2XS: Assets.playSizeXS,
  );

  static const file = SvgSizesIcon(
    pathSizeL: Assets.fileSizeL,
    pathSizeM: Assets.fileSizeM,
    pathSizeS: Assets.fileSizeS,
    pathSizeXS: Assets.fileSizeXS,
    pathSize2XS: Assets.fileSizeXS,
  );

  static const picture = SvgSizesIcon(
    pathSizeL: Assets.pictureSizeL,
    pathSizeM: Assets.pictureSizeM,
    pathSizeS: Assets.pictureSizeS,
    pathSizeXS: Assets.pictureSizeXS,
    pathSize2XS: Assets.pictureSizeXS,
  );

  static const search = SvgSizesIcon(
    pathSizeL: Assets.searchSizeL,
    pathSizeM: Assets.searchSizeM,
    pathSizeS: Assets.searchSizeS,
    pathSizeXS: Assets.searchSizeXS,
    pathSize2XS: Assets.searchSizeXS,
  );

  static const plus = SvgSizesIcon(
    pathSizeL: Assets.plusSizeL,
    pathSizeM: Assets.plusSizeM,
    pathSizeS: Assets.plusSizeS,
    pathSizeXS: Assets.plusSizeXS,
    pathSize2XS: Assets.plusSize2XS,
  );

  static const chatGroup = SvgSizesIcon(
    pathSizeL: Assets.chatGroupSizeL,
    pathSizeM: Assets.chatGroupSizeM,
    pathSizeS: Assets.chatGroupSizeS,
    pathSizeXS: Assets.chatGroupSizeXS,
    pathSize2XS: Assets.chatGroupSizeXS,
  );

  static const time = SvgSizesIcon(
    pathSizeL: Assets.timeSizeL,
    pathSizeM: Assets.timeSizeM,
    pathSizeS: Assets.timeSizeS,
    pathSizeXS: Assets.timeSizeXS,
    pathSize2XS: Assets.timeSizeXS,
  );

  static const reply = SvgSizesIcon(
    pathSizeL: Assets.replySizeL,
    pathSizeM: Assets.replySizeM,
    pathSizeS: Assets.replySizeS,
    pathSizeXS: Assets.replySizeXS,
    pathSize2XS: Assets.replySizeXS,
  );

  static const messageThread = SvgSizesIcon(
    pathSizeL: Assets.messageThreadSizeL,
    pathSizeM: Assets.messageThreadSizeM,
    pathSizeS: Assets.messageThreadSizeS,
    pathSizeXS: Assets.messageThreadSizeXS,
    pathSize2XS: Assets.messageThreadSizeXS,
  );

  static const copy = SvgSizesIcon(
    pathSizeL: Assets.copySizeL,
    pathSizeM: Assets.copySizeM,
    pathSizeS: Assets.copySizeS,
    pathSizeXS: Assets.copySizeXS,
    pathSize2XS: Assets.copySizeXS,
  );

  static const link = SvgSizesIcon(
    pathSizeL: Assets.linkSizeL,
    pathSizeM: Assets.linkSizeM,
    pathSizeS: Assets.linkSizeS,
    pathSizeXS: Assets.linkSizeXS,
    pathSize2XS: Assets.linkSizeXS,
  );

  static const forward = SvgSizesIcon(
    pathSizeL: Assets.forwardSizeL,
    pathSizeM: Assets.forwardSizeM,
    pathSizeS: Assets.forwardSizeS,
    pathSizeXS: Assets.forwardSizeXS,
    pathSize2XS: Assets.forwardSizeXS,
  );

  static const messageUnread = SvgSizesIcon(
    pathSizeL: Assets.messageUnreadSizeL,
    pathSizeM: Assets.messageUnreadSizeM,
    pathSizeS: Assets.messageUnreadSizeS,
    pathSizeXS: Assets.messageUnreadSizeXS,
    pathSize2XS: Assets.messageUnreadSizeXS,
  );

  static const edit = SvgSizesIcon(
    pathSizeL: Assets.editSizeL,
    pathSizeM: Assets.editSizeM,
    pathSizeS: Assets.editSizeS,
    pathSizeXS: Assets.editSizeXS,
    pathSize2XS: Assets.editSizeXS,
  );

  static const trash = SvgSizesIcon(
    pathSizeL: Assets.trashSizeL,
    pathSizeM: Assets.trashSizeM,
    pathSizeS: Assets.trashSizeS,
    pathSizeXS: Assets.trashSizeXS,
    pathSize2XS: Assets.trashSizeXS,
  );

  static const video = SvgSizesIcon(
    pathSizeL: Assets.videoSizeL,
    pathSizeM: Assets.videoSizeM,
    pathSizeS: Assets.videoSizeS,
    pathSizeXS: Assets.videoSizeXS,
    pathSize2XS: Assets.videoSizeXS,
  );

  static const email = SvgSizesIcon(
    pathSizeL: Assets.emailSizeL,
    pathSizeM: Assets.emailSizeM,
    pathSizeS: Assets.emailSizeS,
    pathSizeXS: Assets.emailSizeXS,
    pathSize2XS: Assets.emailSizeXS,
  );

  static const phone = SvgSizesIcon(
    pathSizeL: Assets.phoneSizeL,
    pathSizeM: Assets.phoneSizeM,
    pathSizeS: Assets.phoneSizeS,
    pathSizeXS: Assets.phoneSizeXS,
    pathSize2XS: Assets.phoneSizeXS,
  );

  static const arrowLeft = SvgSizesIcon(
    pathSizeL: Assets.arrowLeftSizeL,
    pathSizeM: Assets.arrowLeftSizeM,
    pathSizeS: Assets.arrowLeftSizeS,
    pathSizeXS: Assets.arrowLeftSizeXS,
    pathSize2XS: Assets.arrowLeftSizeXS,
  );

  static const arrowUp = SvgSizesIcon(
    pathSizeL: Assets.arrowUpSizeL,
    pathSizeM: Assets.arrowUpSizeM,
    pathSizeS: Assets.arrowUpSizeS,
    pathSizeXS: Assets.arrowUpSizeXS,
    pathSize2XS: Assets.arrowUpSizeXS,
  );

  static const arrowDown = SvgSizesIcon(
    pathSizeL: Assets.arrowDownSizeL,
    pathSizeM: Assets.arrowDownSizeM,
    pathSizeS: Assets.arrowDownSizeS,
    pathSizeXS: Assets.arrowDownSizeXS,
    pathSize2XS: Assets.arrowDownSizeXS,
  );

  static const arrowRight = SvgSizesIcon(
    pathSizeL: Assets.arrowRightSizeL,
    pathSizeM: Assets.arrowRightSizeM,
    pathSizeS: Assets.arrowRightSizeS,
    pathSizeXS: Assets.arrowRightSizeXS,
    pathSize2XS: Assets.arrowRightSizeXS,
  );

  static const optionsVertical = SvgSizesIcon(
    pathSizeL: Assets.optionsVerticalSizeL,
    pathSizeM: Assets.optionsVerticalSizeM,
    pathSizeS: Assets.optionsVerticalSizeS,
    pathSizeXS: Assets.optionsVerticalSizeXS,
    pathSize2XS: Assets.optionsVerticalSizeXS,
  );

  static const optionsHorizontal = SvgSizesIcon(
    pathSizeL: Assets.optionsHorizontalSizeL,
    pathSizeM: Assets.optionsHorizontalSizeM,
    pathSizeS: Assets.optionsHorizontalSizeS,
    pathSizeXS: Assets.optionsHorizontalSizeXS,
    pathSize2XS: Assets.optionsHorizontalSizeXS,
  );

  static const notificationMention = SvgSizesIcon(
    pathSizeL: Assets.notificationMentionSizeL,
    pathSizeM: Assets.notificationMentionSizeM,
    pathSizeS: Assets.notificationMentionSizeS,
    pathSizeXS: Assets.notificationMentionSizeXS,
    pathSize2XS: Assets.notificationMentionSizeXS,
  );

  static const notificationOff = SvgSizesIcon(
    pathSizeL: Assets.notificationOffSizeL,
    pathSizeM: Assets.notificationOffSizeM,
    pathSizeS: Assets.notificationOffSizeS,
    pathSizeXS: Assets.notificationOffSizeXS,
    pathSize2XS: Assets.notificationOffSizeXS,
  );

  static const notificationOn = SvgSizesIcon(
    pathSizeL: Assets.notificationOnSizeL,
    pathSizeM: Assets.notificationOnSizeM,
    pathSizeS: Assets.notificationOnSizeS,
    pathSizeXS: Assets.notificationOnSizeXS,
    pathSize2XS: Assets.notificationOnSizeXS,
  );

  static const archive = SvgSizesIcon(
    pathSizeL: Assets.archiveSizeL,
    pathSizeM: Assets.archiveSizeM,
    pathSizeS: Assets.archiveSizeS,
    pathSizeXS: Assets.archiveSizeXS,
    pathSize2XS: Assets.archiveSizeXS,
  );

  static const dearchive = SvgSizesIcon(
    pathSizeL: Assets.dearchiveSizeL,
    pathSizeM: Assets.dearchiveSizeM,
    pathSizeS: Assets.dearchiveSizeS,
    pathSizeXS: Assets.dearchiveSizeXS,
    pathSize2XS: Assets.dearchiveSizeXS,
  );

  static const favoriteYes = SvgSizesIcon(
    pathSizeL: Assets.favoriteYesSizeL,
    pathSizeM: Assets.favoriteYesSizeM,
    pathSizeS: Assets.favoriteYesSizeS,
    pathSizeXS: Assets.favoriteYesSizeXS,
    pathSize2XS: Assets.favoriteYesSizeXS,
  );

  static const favoriteNo = SvgSizesIcon(
    pathSizeL: Assets.favoriteNoSizeL,
    pathSizeM: Assets.favoriteNoSizeM,
    pathSizeS: Assets.favoriteNoSizeS,
    pathSizeXS: Assets.favoriteNoSizeXS,
    pathSize2XS: Assets.favoriteNoSizeXS,
  );

  static const userLeft = SvgSizesIcon(
    pathSizeL: Assets.userLeftSizeL,
    pathSizeM: Assets.userLeftSizeM,
    pathSizeS: Assets.userLeftSizeS,
    pathSizeXS: Assets.userLeftSizeXS,
    pathSize2XS: Assets.userLeftSizeXS,
  );

  static const messageFollow = SvgSizesIcon(
    pathSizeL: Assets.messageFollowSizeL,
    pathSizeM: Assets.messageFollowSizeM,
    pathSizeS: Assets.messageFollowSizeS,
    pathSizeXS: Assets.messageFollowSizeXS,
    pathSize2XS: Assets.messageFollowSizeXS,
  );

  static const userAdminAdd = SvgSizesIcon(
    pathSizeL: Assets.userAdminAddSizeL,
    pathSizeM: Assets.userAdminAddSizeM,
    pathSizeS: Assets.userAdminAddSizeS,
    pathSizeXS: Assets.userAdminAddSizeXS,
    pathSize2XS: Assets.userAdminAddSizeXS,
  );

  static const userAdminDelete = SvgSizesIcon(
    pathSizeL: Assets.userAdminDeleteSizeL,
    pathSizeM: Assets.userAdminDeleteSizeM,
    pathSizeS: Assets.userAdminDeleteSizeS,
    pathSizeXS: Assets.userAdminDeleteSizeXS,
    pathSize2XS: Assets.userAdminDeleteSizeXS,
  );

  static const workspace = SvgSizesIcon(
    pathSizeL: Assets.workspaceSizeL,
    pathSizeM: Assets.workspaceSizeM,
    pathSizeS: Assets.workspaceSizeS,
    pathSizeXS: Assets.workspaceSizeXS,
    pathSize2XS: Assets.workspaceSizeXS,
  );

  static const emojiSad = SvgSizesIcon(
    pathSizeL: Assets.emojiSadSizeL,
    pathSizeM: Assets.emojiSadSizeM,
    pathSizeS: Assets.emojiSadSizeS,
    pathSizeXS: Assets.emojiSadSizeXS,
    pathSize2XS: Assets.emojiSadSizeXS,
  );

  static const user = SvgSizesIcon(
    pathSizeL: Assets.userSizeL,
    pathSizeM: Assets.userSizeM,
    pathSizeS: Assets.userSizeS,
    pathSizeXS: Assets.userSizeXS,
    pathSize2XS: Assets.userSizeXS,
  );

  static const image = SvgSizesIcon(
    pathSizeL: Assets.imageSizeL,
    pathSizeM: Assets.imageSizeM,
    pathSizeS: Assets.imageSizeS,
    pathSizeXS: Assets.imageSizeXS,
    pathSize2XS: Assets.imageSizeXS,
  );

  static const bot = SvgSizesIcon(
    pathSizeL: Assets.botSizeL,
    pathSizeM: Assets.botSizeM,
    pathSizeS: Assets.botSizeS,
    pathSizeXS: Assets.botSizeXS,
    pathSize2XS: Assets.botSizeXS,
  );
}
