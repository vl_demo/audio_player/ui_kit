part of '../../index.dart';

enum APIconType {
  defaultCheck,
  eyeConturClose,
  eyeConturOpen,
  backspace,
  block,
  doorOut,
  doorIn,
  key,
  lock,
  pinLock,
  userGroup,
  cancelDefault,
  emojiGood,
  clip,
  mention,
  groupChat,
  bold,
  channel,
  underline,
  italic,
  pinYes,
  pinNo,
  addEmoji,
  download,
  chevronLeft,
  chevronRight,
  chevronUp,
  chevronDown,
  bookmarkOutline,
  bookmarkFilled,
  favoriteYes,
  favoriteNo,
  penEdit,
  message,
  play,
  file,
  picture,
  search,
  plus,
  chatGroup,
  time,
  reply,
  messageThread,
  copy,
  link,
  forward,
  messageUnread,
  edit,
  trash,
  video,
  email,
  phone,
  arrowLeft,
  arrowUp,
  arrowDown,
  arrowRight,
  optionsVertical,
  optionsHorizontal,
  notificationMention,
  notificationOff,
  notificationOn,
  archive,
  dearchive,
  userLeft,
  messageFollow,
  userAdminAdd,
  userAdminDelete,
  workspace,
  emojiSad,
  user,
  image,
  bot,
  pause,
  stop,
}

class APIconWidget extends StatelessWidget {
  const APIconWidget.size3XS({
    super.key,
    required this.color,
    required this.type,
  }) : size = APSizing.size3XS;

  const APIconWidget.sizeXS({
    super.key,
    required this.color,
    required this.type,
  }) : size = APSizing.sizeXS;

  const APIconWidget.size2XS({
    super.key,
    required this.color,
    required this.type,
  }) : size = APSizing.size2XS;

  const APIconWidget.sizeS({
    super.key,
    required this.color,
    required this.type,
  }) : size = APSizing.sizeS;

  const APIconWidget.size2XM({
    super.key,
    required this.color,
    required this.type,
  }) : size = APSizing.size2XM;

  const APIconWidget.sizeM({
    super.key,
    required this.color,
    required this.type,
  }) : size = APSizing.sizeM;

  const APIconWidget.size2XL({
    super.key,
    required this.color,
    required this.type,
  }) : size = APSizing.size2XL;

  const APIconWidget.size5XL({
    super.key,
    required this.color,
    required this.type,
  }) : size = APSizing.size5XL;

  const APIconWidget.sizeXL({
    super.key,
    required this.color,
    required this.type,
  }) : size = APSizing.sizeXL;

  final APIconType type;
  final double size;
  final Color color;

  @override
  Widget build(BuildContext context) {
    final String svg;

    switch (type) {
      case APIconType.defaultCheck:
        svg = APSvgIconAssets.defaultCheck.sizeL;
        break;
      case APIconType.eyeConturClose:
        svg = APSvgIconAssets.eyeConturClose.sizeL;
        break;
      case APIconType.eyeConturOpen:
        svg = APSvgIconAssets.eyeConturOpen.sizeL;
        break;
      case APIconType.backspace:
        svg = APSvgIconAssets.backspace.sizeL;
        break;
      case APIconType.block:
        svg = APSvgIconAssets.block.sizeL;
        break;
      case APIconType.doorOut:
        svg = APSvgIconAssets.doorOut.sizeL;
        break;
      case APIconType.doorIn:
        svg = APSvgIconAssets.doorIn.sizeL;
        break;
      case APIconType.key:
        svg = APSvgIconAssets.key.sizeL;
        break;
      case APIconType.lock:
        svg = APSvgIconAssets.lock.sizeL;
        break;
      case APIconType.pinLock:
        svg = APSvgIconAssets.pinLock.sizeL;
        break;
      case APIconType.userGroup:
        svg = APSvgIconAssets.userGroup.sizeL;
        break;
      case APIconType.cancelDefault:
        svg = APSvgIconAssets.cancelDefault.sizeL;
        break;
      case APIconType.emojiGood:
        svg = APSvgIconAssets.emojiGood.sizeL;
        break;
      case APIconType.clip:
        svg = APSvgIconAssets.clip.sizeL;
        break;
      case APIconType.mention:
        svg = APSvgIconAssets.mention.sizeL;
        break;
      case APIconType.groupChat:
        svg = APSvgIconAssets.groupChat.sizeL;
        break;
      case APIconType.bold:
        svg = APSvgIconAssets.bold.sizeL;
        break;
      case APIconType.channel:
        svg = APSvgIconAssets.channel.sizeL;
        break;
      case APIconType.underline:
        svg = APSvgIconAssets.underline.sizeL;
        break;
      case APIconType.italic:
        svg = APSvgIconAssets.italic.sizeL;
        break;
      case APIconType.pinYes:
        svg = APSvgIconAssets.pinYes.sizeL;
        break;
      case APIconType.pinNo:
        svg = APSvgIconAssets.pinNo.sizeL;
        break;
      case APIconType.addEmoji:
        svg = APSvgIconAssets.addEmoji.sizeL;
        break;
      case APIconType.download:
        svg = APSvgIconAssets.download.sizeL;
        break;
      case APIconType.chevronLeft:
        svg = APSvgIconAssets.chevronLeft.sizeL;
        break;
      case APIconType.chevronRight:
        svg = APSvgIconAssets.chevronRight.sizeL;
        break;
      case APIconType.chevronUp:
        svg = APSvgIconAssets.chevronUp.sizeL;
        break;
      case APIconType.chevronDown:
        svg = APSvgIconAssets.chevronDown.sizeL;
        break;
      case APIconType.bookmarkOutline:
        svg = APSvgIconAssets.bookmarkOutline.sizeL;
        break;
      case APIconType.bookmarkFilled:
        svg = APSvgIconAssets.bookmarkFilled.sizeL;
        break;
      case APIconType.penEdit:
        svg = APSvgIconAssets.penEdit.sizeL;
        break;
      case APIconType.message:
        svg = APSvgIconAssets.message.sizeL;
        break;
/*      case APIconType.play:
        svg = APSvgIconAssets.play.sizeL;
        break;*/
      case APIconType.file:
        svg = APSvgIconAssets.file.sizeL;
        break;
      case APIconType.picture:
        svg = APSvgIconAssets.picture.sizeL;
        break;
      case APIconType.search:
        svg = APSvgIconAssets.search.sizeL;
        break;
      case APIconType.plus:
        svg = APSvgIconAssets.plus.sizeL;
        break;
      case APIconType.chatGroup:
        svg = APSvgIconAssets.chatGroup.sizeL;
        break;
      case APIconType.time:
        svg = APSvgIconAssets.time.sizeL;
        break;
      case APIconType.reply:
        svg = APSvgIconAssets.reply.sizeL;
        break;
      case APIconType.messageThread:
        svg = APSvgIconAssets.messageThread.sizeL;
        break;
      case APIconType.copy:
        svg = APSvgIconAssets.copy.sizeL;
        break;
      case APIconType.link:
        svg = APSvgIconAssets.link.sizeL;
        break;
      case APIconType.forward:
        svg = APSvgIconAssets.forward.sizeL;
        break;
      case APIconType.messageUnread:
        svg = APSvgIconAssets.messageUnread.sizeL;
        break;
      case APIconType.edit:
        svg = APSvgIconAssets.edit.sizeL;
        break;
      case APIconType.trash:
        svg = APSvgIconAssets.trash.sizeL;
        break;
      case APIconType.video:
        svg = APSvgIconAssets.video.sizeL;
        break;
      case APIconType.email:
        svg = APSvgIconAssets.email.sizeL;
        break;
      case APIconType.phone:
        svg = APSvgIconAssets.phone.sizeL;
        break;
      case APIconType.arrowLeft:
        svg = APSvgIconAssets.arrowLeft.sizeL;
        break;
      case APIconType.arrowUp:
        svg = APSvgIconAssets.arrowUp.sizeL;
        break;
      case APIconType.arrowDown:
        svg = APSvgIconAssets.arrowDown.sizeL;
        break;
      case APIconType.arrowRight:
        svg = APSvgIconAssets.arrowRight.sizeL;
        break;
      case APIconType.optionsVertical:
        svg = APSvgIconAssets.optionsVertical.sizeL;
        break;
      case APIconType.optionsHorizontal:
        svg = APSvgIconAssets.optionsHorizontal.sizeL;
        break;
      case APIconType.notificationMention:
        svg = APSvgIconAssets.notificationMention.sizeL;
        break;
      case APIconType.notificationOff:
        svg = APSvgIconAssets.notificationOff.sizeL;
        break;
      case APIconType.notificationOn:
        svg = APSvgIconAssets.notificationOn.sizeL;
        break;
      case APIconType.archive:
        svg = APSvgIconAssets.archive.sizeL;
        break;
      case APIconType.dearchive:
        svg = APSvgIconAssets.dearchive.sizeL;
        break;
      case APIconType.favoriteYes:
        svg = APSvgIconAssets.favoriteYes.sizeL;
        break;
      case APIconType.favoriteNo:
        svg = APSvgIconAssets.favoriteNo.sizeL;
        break;
      case APIconType.userLeft:
        svg = APSvgIconAssets.userLeft.sizeL;
        break;
      case APIconType.messageFollow:
        svg = APSvgIconAssets.messageFollow.sizeL;
        break;
      case APIconType.userAdminAdd:
        svg = APSvgIconAssets.userAdminAdd.sizeL;
        break;
      case APIconType.userAdminDelete:
        svg = APSvgIconAssets.userAdminDelete.sizeL;
        break;
      case APIconType.workspace:
        svg = APSvgIconAssets.workspace.sizeL;
        break;
      case APIconType.emojiSad:
        svg = APSvgIconAssets.emojiSad.sizeL;
        break;
      case APIconType.user:
        svg = APSvgIconAssets.user.sizeL;
        break;
      case APIconType.image:
        svg = APSvgIconAssets.image.sizeL;
        break;
      case APIconType.bot:
        svg = APSvgIconAssets.bot.sizeL;
        break;
      default:
        final IconData icon;

        switch (type) {
          case APIconType.play:
            icon = Icons.play_arrow;
            break;
          case APIconType.pause:
            icon = Icons.pause;
            break;
          case APIconType.stop:
            icon = Icons.stop;
            break;
          default:
            icon = Icons.accessibility_new_outlined;
            break;
        }
        return Icon(
          icon,
          color: color,
          size: size,
        );
    }

    return SvgPicture.asset(
      svg,
      width: size,
      height: size,
      color: color,
    );
  }
}
