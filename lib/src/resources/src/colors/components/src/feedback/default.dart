part of '../../index.dart';

class FeedbackDefaultColors extends ThemeExtension<FeedbackDefaultColors> {
  FeedbackDefaultColors({
    required this.defaultFill,
    required this.defaultText,
    required this.defaultIcon,
  });

  final Color defaultFill;
  final Color defaultText;
  final Color defaultIcon;

  @override
  FeedbackDefaultColors lerp(FeedbackDefaultColors? other, double t) {
    if (other is! FeedbackDefaultColors) {
      return this;
    }

    return FeedbackDefaultColors(
      defaultFill: Color.lerp(defaultFill, other.defaultFill, t)!,
      defaultText: Color.lerp(defaultText, other.defaultText, t)!,
      defaultIcon: Color.lerp(defaultIcon, other.defaultIcon, t)!,
    );
  }

  @override
  FeedbackDefaultColors copyWith({
    Color? defaultFill,
    Color? defaultText,
    Color? defaultIcon,
  }) {
    return FeedbackDefaultColors(
      defaultFill: defaultFill ?? this.defaultFill,
      defaultText: defaultText ?? this.defaultText,
      defaultIcon: defaultIcon ?? this.defaultIcon,
    );
  }
}
