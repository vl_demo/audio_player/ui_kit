part of '../../index.dart';

class FeedbackInvertColors extends ThemeExtension<FeedbackInvertColors> {
  FeedbackInvertColors({
    required this.defaultFill,
    required this.hoverFill,
    required this.pressedFill,
    required this.defaultText,
    required this.hoverText,
    required this.pressedText,
    required this.defaultIcon,
    required this.hoverIcon,
    required this.pressedIcon,
  });

  final Color defaultFill;
  final Color hoverFill;
  final Color pressedFill;
  final Color defaultText;
  final Color hoverText;
  final Color pressedText;
  final Color defaultIcon;
  final Color hoverIcon;
  final Color pressedIcon;

  @override
  FeedbackInvertColors lerp(FeedbackInvertColors? other, double t) {
    if (other is! FeedbackInvertColors) {
      return this;
    }

    return FeedbackInvertColors(
      defaultFill: Color.lerp(defaultFill, other.defaultFill, t)!,
      hoverFill: Color.lerp(hoverFill, other.hoverFill, t)!,
      pressedFill: Color.lerp(pressedFill, other.pressedFill, t)!,
      defaultText: Color.lerp(defaultText, other.defaultText, t)!,
      hoverText: Color.lerp(hoverText, other.hoverText, t)!,
      pressedText: Color.lerp(pressedText, other.pressedText, t)!,
      defaultIcon: Color.lerp(defaultIcon, other.defaultIcon, t)!,
      hoverIcon: Color.lerp(hoverIcon, other.hoverIcon, t)!,
      pressedIcon: Color.lerp(pressedIcon, other.pressedIcon, t)!,
    );
  }

  @override
  FeedbackInvertColors copyWith({
    Color? defaultFill,
    Color? hoverFill,
    Color? pressedFill,
    Color? defaultText,
    Color? hoverText,
    Color? pressedText,
    Color? defaultIcon,
    Color? hoverIcon,
    Color? pressedIcon,
  }) {
    return FeedbackInvertColors(
      defaultFill: defaultFill ?? this.defaultFill,
      hoverFill: hoverFill ?? this.hoverFill,
      pressedFill: pressedFill ?? this.pressedFill,
      defaultText: defaultText ?? this.defaultText,
      hoverText: hoverText ?? this.hoverText,
      pressedText: pressedText ?? this.pressedText,
      defaultIcon: defaultIcon ?? this.defaultIcon,
      hoverIcon: hoverIcon ?? this.hoverIcon,
      pressedIcon: pressedIcon ?? this.pressedIcon,
    );
  }
}
