part of '../../index.dart';

class WorkspaceCardDeselectedColors
    extends ThemeExtension<WorkspaceCardDeselectedColors> {
  WorkspaceCardDeselectedColors({
    required this.defaultFill,
    required this.hoverFill,
    required this.pressedFill,
    required this.disabledFill,
    required this.focusedFill,
    required this.defaultBorder,
    required this.hoverBorder,
    required this.pressedBorder,
    required this.disabledBorder,
    required this.focusedBorder,
    required this.focusedBorder2,
  });

  final Color defaultFill;
  final Color hoverFill;
  final Color pressedFill;
  final Color disabledFill;
  final Color focusedFill;

  final Color defaultBorder;
  final Color hoverBorder;
  final Color pressedBorder;
  final Color disabledBorder;
  final Color focusedBorder;

  final Color focusedBorder2;

  @override
  WorkspaceCardDeselectedColors lerp(
      WorkspaceCardDeselectedColors? other, double t) {
    if (other is! WorkspaceCardDeselectedColors) {
      return this;
    }

    return WorkspaceCardDeselectedColors(
      defaultFill: Color.lerp(defaultFill, other.defaultFill, t)!,
      hoverFill: Color.lerp(hoverFill, other.hoverFill, t)!,
      pressedFill: Color.lerp(pressedFill, other.pressedFill, t)!,
      disabledFill: Color.lerp(disabledFill, other.disabledFill, t)!,
      focusedFill: Color.lerp(focusedFill, other.focusedFill, t)!,
      defaultBorder: Color.lerp(defaultBorder, other.defaultBorder, t)!,
      hoverBorder: Color.lerp(hoverBorder, other.hoverBorder, t)!,
      pressedBorder: Color.lerp(pressedBorder, other.pressedBorder, t)!,
      disabledBorder: Color.lerp(disabledBorder, other.disabledBorder, t)!,
      focusedBorder: Color.lerp(focusedBorder, other.focusedBorder, t)!,
      focusedBorder2: Color.lerp(focusedBorder2, other.focusedBorder2, t)!,
    );
  }

  @override
  WorkspaceCardDeselectedColors copyWith({
    Color? defaultFill,
    Color? hoverFill,
    Color? pressedFill,
    Color? disabledFill,
    Color? focusedFill,
    Color? defaultBorder,
    Color? hoverBorder,
    Color? pressedBorder,
    Color? disabledBorder,
    Color? focusedBorder,
    Color? focusedBorder2,
  }) {
    return WorkspaceCardDeselectedColors(
      defaultFill: defaultFill ?? this.defaultFill,
      hoverFill: hoverFill ?? this.hoverFill,
      pressedFill: pressedFill ?? this.pressedFill,
      disabledFill: disabledFill ?? this.disabledFill,
      focusedFill: focusedFill ?? this.focusedFill,
      defaultBorder: defaultBorder ?? this.defaultBorder,
      hoverBorder: hoverBorder ?? this.hoverBorder,
      pressedBorder: pressedBorder ?? this.pressedBorder,
      disabledBorder: disabledBorder ?? this.disabledBorder,
      focusedBorder: focusedBorder ?? this.focusedBorder,
      focusedBorder2: focusedBorder2 ?? this.focusedBorder2,
    );
  }
}
