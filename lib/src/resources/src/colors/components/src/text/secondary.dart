part of '../../index.dart';

class TextSecondaryColors extends ThemeExtension<TextSecondaryColors> {
  TextSecondaryColors({
    required this.secondary100,
    required this.secondary80,
    required this.secondary70,
    required this.secondary60,
    required this.secondary40,
    required this.secondary0,
  });

  final Color secondary100;
  final Color secondary80;
  final Color secondary70;
  final Color secondary60;
  final Color secondary40;
  final Color secondary0;

  @override
  TextSecondaryColors lerp(TextSecondaryColors? other, double t) {
    if (other is! TextSecondaryColors) {
      return this;
    }

    return TextSecondaryColors(
      secondary100: Color.lerp(secondary100, other.secondary100, t)!,
      secondary80: Color.lerp(secondary80, other.secondary80, t)!,
      secondary70: Color.lerp(secondary70, other.secondary70, t)!,
      secondary60: Color.lerp(secondary60, other.secondary60, t)!,
      secondary40: Color.lerp(secondary40, other.secondary40, t)!,
      secondary0: Color.lerp(secondary0, other.secondary0, t)!,
    );
  }

  @override
  TextSecondaryColors copyWith({
    Color? secondary100,
    Color? secondary80,
    Color? secondary70,
    Color? secondary50,
    Color? secondary30,
    Color? secondary0,
  }) {
    return TextSecondaryColors(
      secondary100: secondary100 ?? this.secondary100,
      secondary80: secondary70 ?? this.secondary80,
      secondary70: secondary70 ?? this.secondary70,
      secondary60: secondary50 ?? this.secondary60,
      secondary40: secondary30 ?? this.secondary40,
      secondary0: secondary0 ?? this.secondary0,
    );
  }
}
