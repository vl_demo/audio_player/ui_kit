part of '../../index.dart';

class TextSystemColors extends ThemeExtension<TextSystemColors> {
  TextSystemColors({
    required this.good,
    required this.bad,
    required this.warning,
    required this.info,
    required this.attention,
  });

  final Color good;
  final Color bad;
  final Color warning;
  final Color info;
  final Color attention;

  @override
  TextSystemColors lerp(TextSystemColors? other, double t) {
    if (other is! TextSystemColors) {
      return this;
    }

    return TextSystemColors(
      good: Color.lerp(good, other.good, t)!,
      bad: Color.lerp(bad, other.bad, t)!,
      warning: Color.lerp(warning, other.warning, t)!,
      info: Color.lerp(info, other.info, t)!,
      attention: Color.lerp(attention, other.attention, t)!,
    );
  }

  @override
  TextSystemColors copyWith({
    Color? good,
    Color? bad,
    Color? warning,
    Color? info,
    Color? attention,
  }) {
    return TextSystemColors(
      good: good ?? this.good,
      bad: bad ?? this.bad,
      warning: warning ?? this.warning,
      info: info ?? this.info,
      attention: attention ?? this.attention,
    );
  }
}
