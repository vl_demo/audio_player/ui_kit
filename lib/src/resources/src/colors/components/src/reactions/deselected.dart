part of '../../index.dart';

class ReactionsDeselectedColors
    extends ThemeExtension<ReactionsDeselectedColors> {
  ReactionsDeselectedColors({
    required this.defaultFill,
    required this.hoverFill,
    required this.defaultBorder,
    required this.hoverBorder,
    required this.defaultText,
    required this.hoverText,
    required this.defaultIcon,
    required this.hoverIcon,
  });

  final Color defaultFill;
  final Color hoverFill;
  final Color defaultBorder;
  final Color hoverBorder;
  final Color defaultText;
  final Color hoverText;
  final Color defaultIcon;
  final Color hoverIcon;

  @override
  ReactionsDeselectedColors lerp(ReactionsDeselectedColors? other, double t) {
    if (other is! ReactionsDeselectedColors) {
      return this;
    }

    return ReactionsDeselectedColors(
      defaultFill: Color.lerp(defaultFill, other.defaultFill, t)!,
      hoverFill: Color.lerp(hoverFill, other.hoverFill, t)!,
      defaultBorder: Color.lerp(defaultBorder, other.defaultBorder, t)!,
      hoverBorder: Color.lerp(hoverBorder, other.hoverBorder, t)!,
      defaultText: Color.lerp(defaultText, other.defaultText, t)!,
      hoverText: Color.lerp(hoverText, other.hoverText, t)!,
      defaultIcon: Color.lerp(defaultIcon, other.defaultIcon, t)!,
      hoverIcon: Color.lerp(hoverIcon, other.hoverIcon, t)!,
    );
  }

  @override
  ReactionsDeselectedColors copyWith({
    Color? defaultFill,
    Color? hoverFill,
    Color? defaultBorder,
    Color? hoverBorder,
    Color? defaultText,
    Color? hoverText,
    Color? defaultIcon,
    Color? hoverIcon,
  }) {
    return ReactionsDeselectedColors(
      defaultFill: defaultFill ?? this.defaultFill,
      hoverFill: hoverFill ?? this.hoverFill,
      defaultBorder: defaultBorder ?? this.defaultBorder,
      hoverBorder: hoverBorder ?? this.hoverBorder,
      defaultText: defaultText ?? this.defaultText,
      hoverText: hoverText ?? this.hoverText,
      defaultIcon: defaultIcon ?? this.defaultIcon,
      hoverIcon: hoverIcon ?? this.hoverIcon,
    );
  }
}
