part of '../../index.dart';

class SelectorSelectedColors extends ThemeExtension<SelectorSelectedColors> {
  SelectorSelectedColors({
    required this.defaultFill,
    required this.hoverFill,
    required this.pressedFill,
    required this.disabledFill,
    required this.focusedFill,
    required this.goodFill,
    required this.badFill,
    required this.focusedBorder2,
    required this.defaultIcon,
    required this.hoverIcon,
    required this.pressedIcon,
    required this.disabledIcon,
    required this.focusedIcon,
    required this.goodIcon,
    required this.badIcon,
  });

  final Color defaultFill;
  final Color hoverFill;
  final Color pressedFill;
  final Color disabledFill;
  final Color focusedFill;
  final Color goodFill;
  final Color badFill;
  final Color focusedBorder2;
  final Color defaultIcon;
  final Color hoverIcon;
  final Color pressedIcon;
  final Color disabledIcon;
  final Color focusedIcon;
  final Color goodIcon;
  final Color badIcon;

  @override
  SelectorSelectedColors lerp(SelectorSelectedColors? other, double t) {
    if (other is! SelectorSelectedColors) {
      return this;
    }

    return SelectorSelectedColors(
      defaultFill: Color.lerp(defaultFill, other.defaultFill, t)!,
      hoverFill: Color.lerp(hoverFill, other.hoverFill, t)!,
      pressedFill: Color.lerp(pressedFill, other.pressedFill, t)!,
      disabledFill: Color.lerp(disabledFill, other.disabledFill, t)!,
      focusedFill: Color.lerp(focusedFill, other.focusedFill, t)!,
      goodFill: Color.lerp(goodFill, other.goodFill, t)!,
      badFill: Color.lerp(badFill, other.badFill, t)!,
      focusedBorder2: Color.lerp(focusedBorder2, other.focusedBorder2, t)!,
      defaultIcon: Color.lerp(defaultIcon, other.defaultIcon, t)!,
      hoverIcon: Color.lerp(hoverIcon, other.hoverIcon, t)!,
      pressedIcon: Color.lerp(pressedIcon, other.pressedIcon, t)!,
      disabledIcon: Color.lerp(disabledIcon, other.disabledIcon, t)!,
      focusedIcon: Color.lerp(focusedIcon, other.focusedIcon, t)!,
      goodIcon: Color.lerp(goodIcon, other.goodIcon, t)!,
      badIcon: Color.lerp(badIcon, other.badIcon, t)!,
    );
  }

  @override
  SelectorSelectedColors copyWith({
    Color? defaultFill,
    Color? hoverFill,
    Color? pressedFill,
    Color? disabledFill,
    Color? focusedFill,
    Color? goodFill,
    Color? badFill,
    Color? focusedBorder2,
    Color? defaultIcon,
    Color? hoverIcon,
    Color? pressedIcon,
    Color? disabledIcon,
    Color? focusedIcon,
    Color? goodIcon,
    Color? badIcon,
  }) {
    return SelectorSelectedColors(
      defaultFill: defaultFill ?? this.defaultFill,
      hoverFill: hoverFill ?? this.hoverFill,
      pressedFill: pressedFill ?? this.pressedFill,
      disabledFill: disabledFill ?? this.disabledFill,
      focusedFill: focusedFill ?? this.focusedFill,
      goodFill: goodFill ?? this.goodFill,
      badFill: badFill ?? this.badFill,
      focusedBorder2: focusedBorder2 ?? this.focusedBorder2,
      defaultIcon: defaultIcon ?? this.defaultIcon,
      hoverIcon: hoverIcon ?? this.hoverIcon,
      pressedIcon: pressedIcon ?? this.pressedIcon,
      disabledIcon: disabledIcon ?? this.disabledIcon,
      focusedIcon: focusedIcon ?? this.focusedIcon,
      goodIcon: goodIcon ?? this.goodIcon,
      badIcon: badIcon ?? this.badIcon,
    );
  }
}
