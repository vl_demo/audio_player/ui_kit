part of '../../index.dart';

class DropdownListSelectedColors
    extends ThemeExtension<DropdownListSelectedColors> {
  DropdownListSelectedColors({
    required this.defaultFill,
    required this.hoverFill,
    required this.focusedFill,
    required this.focusedBorder,
    required this.defaultText,
    required this.hoverText,
    required this.focusedText,
    required this.defaultIcon,
    required this.hoverIcon,
    required this.focusedIcon,
  });

  final Color defaultFill;
  final Color hoverFill;
  final Color focusedFill;
  final Color focusedBorder;
  final Color defaultText;
  final Color hoverText;
  final Color focusedText;
  final Color defaultIcon;
  final Color hoverIcon;
  final Color focusedIcon;

  @override
  DropdownListSelectedColors lerp(DropdownListSelectedColors? other, double t) {
    if (other is! DropdownListSelectedColors) {
      return this;
    }

    return DropdownListSelectedColors(
      defaultFill: Color.lerp(defaultFill, other.defaultFill, t)!,
      hoverFill: Color.lerp(hoverFill, other.hoverFill, t)!,
      focusedFill: Color.lerp(focusedFill, other.focusedFill, t)!,
      focusedBorder: Color.lerp(focusedBorder, other.focusedBorder, t)!,
      defaultText: Color.lerp(defaultText, other.defaultText, t)!,
      hoverText: Color.lerp(hoverText, other.hoverText, t)!,
      focusedText: Color.lerp(focusedText, other.focusedText, t)!,
      defaultIcon: Color.lerp(defaultIcon, other.defaultIcon, t)!,
      hoverIcon: Color.lerp(hoverIcon, other.hoverIcon, t)!,
      focusedIcon: Color.lerp(focusedIcon, other.focusedIcon, t)!,
    );
  }

  @override
  DropdownListSelectedColors copyWith({
    Color? defaultFill,
    Color? hoverFill,
    Color? focusedFill,
    Color? focusedBorder,
    Color? defaultText,
    Color? hoverText,
    Color? focusedText,
    Color? defaultIcon,
    Color? hoverIcon,
    Color? focusedIcon,
  }) {
    return DropdownListSelectedColors(
      defaultFill: defaultFill ?? this.defaultFill,
      hoverFill: hoverFill ?? this.hoverFill,
      focusedFill: focusedFill ?? this.focusedFill,
      focusedBorder: focusedBorder ?? this.focusedBorder,
      defaultText: defaultText ?? this.defaultText,
      hoverText: hoverText ?? this.hoverText,
      focusedText: focusedText ?? this.focusedText,
      defaultIcon: defaultIcon ?? this.defaultIcon,
      hoverIcon: hoverIcon ?? this.hoverIcon,
      focusedIcon: focusedIcon ?? this.focusedIcon,
    );
  }
}
