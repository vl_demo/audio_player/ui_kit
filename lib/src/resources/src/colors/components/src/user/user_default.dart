part of '../../index.dart';

class UserDefaultColors extends ThemeExtension<UserDefaultColors> {
  UserDefaultColors({
    required this.text,
  });

  final Color text;

  @override
  UserDefaultColors lerp(UserDefaultColors? other, double t) {
    if (other is! UserDefaultColors) {
      return this;
    }

    return UserDefaultColors(
      text: Color.lerp(text, other.text, t)!,
    );
  }

  @override
  UserDefaultColors copyWith({
    Color? text,
  }) {
    return UserDefaultColors(
      text: text ?? this.text,
    );
  }
}
