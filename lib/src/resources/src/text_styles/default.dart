part of '../../index.dart';

class MobileFontStyles {
  static const fontFamily = 'packages/ap_ui_kit/Ubuntu';

  static const fontMobileHeadlineH1 = TextStyle(
    color: DefaultLightBaseTokenColors.secondary100,
    fontStyle: FontStyle.normal,
    fontSize: FontSizes.fontSizeXL,
    fontWeight: FontWeight.bold,
    fontFamily: fontFamily,
    height: LineHeights.lineHeightL / FontSizes.fontSizeXL,
  );
  static const fontMobileHeadlineH2 = TextStyle(
    color: DefaultLightBaseTokenColors.secondary100,
    fontStyle: FontStyle.normal,
    fontSize: FontSizes.fontSizeL,
    fontWeight: FontWeight.bold,
    fontFamily: fontFamily,
    height: LineHeights.lineHeightS / FontSizes.fontSizeL,
  );
  static const fontMobileHeadlineH3 = TextStyle(
    color: DefaultLightBaseTokenColors.secondary100,
    fontStyle: FontStyle.normal,
    fontSize: FontSizes.fontSizeS,
    fontWeight: FontWeight.bold,
    fontFamily: fontFamily,
    height: LineHeights.lineHeightXS / FontSizes.fontSizeS,
  );
  static const fontMobileHeadlineH4 = TextStyle(
    color: DefaultLightBaseTokenColors.secondary100,
    fontStyle: FontStyle.normal,
    fontSize: FontSizes.fontSizeXS,
    fontWeight: FontWeight.w500,
    fontFamily: fontFamily,
    height: LineHeights.lineHeight2XS / FontSizes.fontSizeXS,
  );
  static const fontMobileHeadlineH5 = TextStyle(
    color: DefaultLightBaseTokenColors.secondary100,
    fontStyle: FontStyle.normal,
    fontSize: FontSizes.fontSize2XS,
    fontWeight: FontWeight.w500,
    fontFamily: fontFamily,
    height: LineHeights.lineHeight2XS / FontSizes.fontSize2XS,
  );
  static const fontMobileBodyL = TextStyle(
    color: DefaultLightBaseTokenColors.secondary100,
    fontStyle: FontStyle.normal,
    fontSize: FontSizes.fontSizeXS,
    fontWeight: FontWeight.w400,
    fontFamily: fontFamily,
    height: LineHeights.lineHeight2XS / FontSizes.fontSizeXS,
  );
  static const fontMobileBodyM = TextStyle(
    color: DefaultLightBaseTokenColors.secondary100,
    fontStyle: FontStyle.normal,
    fontSize: FontSizes.fontSize2XS,
    fontWeight: FontWeight.w400,
    fontFamily: fontFamily,
    height: LineHeights.lineHeight2XS / FontSizes.fontSize2XS,
  );
  static const fontMobileBodyS = TextStyle(
    color: DefaultLightBaseTokenColors.secondary100,
    fontStyle: FontStyle.normal,
    fontSize: FontSizes.fontSize3XS,
    fontWeight: FontWeight.w400,
    fontFamily: fontFamily,
    height: LineHeights.lineHeight2XS / FontSizes.fontSize3XS,
  );
  static const fontMobileCaptionM = TextStyle(
    color: DefaultLightBaseTokenColors.secondary100,
    fontStyle: FontStyle.normal,
    fontSize: FontSizes.fontSize3XS,
    fontWeight: FontWeight.w500,
    fontFamily: fontFamily,
    height: LineHeights.lineHeight2XS / FontSizes.fontSize3XS,
  );
  static const fontMobileCaptionS = TextStyle(
    color: DefaultLightBaseTokenColors.secondary100,
    fontStyle: FontStyle.normal,
    fontSize: FontSizes.fontSize4XS,
    fontWeight: FontWeight.bold,
    fontFamily: fontFamily,
    height: LineHeights.lineHeight3XS / FontSizes.fontSize4XS,
  );
}

class ButtonFontStyles {
  static const fontFamily = 'Ubuntu';

  static const fontControlButtonXL = TextStyle(
    color: DefaultLightBaseTokenColors.secondary0,
    fontStyle: FontStyle.normal,
    fontSize: FontSizes.fontSize3XL,
    fontWeight: FontWeight.w400,
    fontFamily: fontFamily,
    height: LineHeights.lineHeightL / FontSizes.fontSize3XL,
  );

  static const fontControlButtonL = TextStyle(
    color: DefaultLightBaseTokenColors.secondary0,
    fontStyle: FontStyle.normal,
    fontSize: FontSizes.fontSize2XS,
    fontWeight: FontWeight.w400,
    fontFamily: fontFamily,
  );

  static const fontControlButtonS = TextStyle(
    color: DefaultLightBaseTokenColors.secondary0,
    fontStyle: FontStyle.normal,
    fontSize: FontSizes.fontSize3XS,
    fontWeight: FontWeight.w400,
    fontFamily: fontFamily,
  );
}
