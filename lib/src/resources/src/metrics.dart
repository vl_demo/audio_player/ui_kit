part of '../index.dart';

class APSpacing {
  static const spacing4XS = 2.0;
  static const spacing3XS = 4.0;
  static const spacing2XS = 8.0;
  static const spacingXS = 12.0;
  static const spacingS = 16.0;
  static const spacing2XM = 24.0;
  static const spacingXM = 32.0;
  static const spacingM = 40.0;
  static const spacingL = 48.0;
  static const spacingXL = 56.0;
  static const spacing2XL = 64.0;
  static const spacing3XL = 72.0;
  static const spacing4XL = 96.0;
  static const spacing5XL = 124.0;
}

class APButtonShapes {
  static const radius4XS = RoundedRectangleBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(2.0),
    ),
  );
  static const radius3XS = RoundedRectangleBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(4.0),
    ),
  );
  static const radius2XS = RoundedRectangleBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(8.0),
    ),
  );
  static const radiusXS = RoundedRectangleBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(10.0),
    ),
  );
  static const radiusM = RoundedRectangleBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(16.0),
    ),
  );
  static const radiusL = RoundedRectangleBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(20.0),
    ),
  );
  static const radiusXL = RoundedRectangleBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(24.0),
    ),
  );
  static const radius2XL = RoundedRectangleBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(32.0),
    ),
  );
}

class APRadiuses {
  static const radius4XS = 2.0;
  static const radius3XS = 4.0;
  static const radius2XS = 8.0;
  static const radiusXS = 10.0;
  static const radiusM = 16.0;
  static const radiusL = 20.0;
  static const radiusXL = 24.0;
  static const radius2XL = 32.0;
}

class APSizing {
  static const size6XS = 1.0;
  static const size5XS = 2.0;
  static const size4XS = 4.0;
  static const size3XS = 8.0;
  static const size2XS = 12.0;
  static const sizeXS = 16.0;
  static const sizeS = 20.0;
  static const size2XM = 24.0;
  static const sizeXM = 28.0;
  static const sizeM = 32.0;
  static const sizeL = 36.0;
  static const sizeXL = 40.0;
  static const size2XL = 48.0;
  static const size3XL = 56.0;
  static const size4XL = 64.0;
  static const size5XL = 72.0;
  static const size6XL = 104.0;
  static const size7XL = 176.0;
}
