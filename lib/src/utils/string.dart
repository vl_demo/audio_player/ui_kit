extension PackagePrefix on String {
  String get packagePrefix {
    return 'packages/ap_ui_kit/$this';
  }
}
