import 'package:ap_ui_kit/ap_ui_kit.dart';
import 'package:flutter/material.dart';

class APPlayButton extends StatelessWidget {
  const APPlayButton({
    super.key,
    required this.stateType,
    required this.onPlayTap,
  });

  final APSongProgressStateType stateType;
  final void Function(APSongProgressStateType stateType) onPlayTap;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final buttonPrimaryColors = theme.extension<ButtonPrimaryColors>()!;

    final APIconType iconType;

    switch (stateType) {
      case APSongProgressStateType.play:
        iconType = APIconType.play;
        break;
      case APSongProgressStateType.pause:
        iconType = APIconType.pause;
        break;
      case APSongProgressStateType.stop:
        iconType = APIconType.stop;
        break;
    }

    return GestureDetector(
      onTap: () {
        onPlayTap(stateType);
      },
      child: SizedBox(
        width: APSizing.size2XL,
        height: APSizing.size2XL,
        child: DecoratedBox(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: buttonPrimaryColors.defaultFill,
          ),
          child: APIconWidget.sizeM(
            color: buttonPrimaryColors.defaultIcon,
            type: iconType,
          ),
        ),
      ),
    );
  }
}
