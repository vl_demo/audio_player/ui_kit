import 'package:ap_ui_kit/ap_ui_kit.dart';
import 'package:ap_ui_kit/generated/assets.dart';
import 'package:flutter/material.dart';

class APSongImage extends StatelessWidget {
  const APSongImage({
    super.key,
    this.imagePath,
  });

  final String? imagePath;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final dividerSecondaryColors = theme.extension<DividerSecondaryColors>()!;

    return DecoratedBox(
      decoration: BoxDecoration(
        border: Border.all(
          color: dividerSecondaryColors.secondary50,
          width: APSizing.size6XS,
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(
            APRadiuses.radiusXS + APSizing.size6XS,
          ),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(
          APSizing.size6XS,
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(
            APRadiuses.radiusXS,
          ),
          child: Image.asset(
            width: APSizing.size4XL,
            height: APSizing.size4XL,
            imagePath ?? Assets.pngSongPlaceholder.packagePrefix,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
