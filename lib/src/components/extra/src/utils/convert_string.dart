import 'package:intl/intl.dart';

class APConvertString {
  static String secondsToString(int seconds) {
    final numberFormat = NumberFormat("00", "en_US");

    final s = seconds ~/ 60;
    return '${numberFormat.format(s)}:${numberFormat.format(seconds - s * 60)}';
  }
}
