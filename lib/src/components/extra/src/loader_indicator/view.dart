import 'package:ap_ui_kit/ap_ui_kit.dart';
import 'package:flutter/material.dart';

enum APLoaderIndicatorSize {
  lg,
  sm,
}

class APLoaderIndicator extends StatefulWidget {
  const APLoaderIndicator({
    super.key,
  }) : indicatorSize = APLoaderIndicatorSize.lg;

  const APLoaderIndicator.sm({
    super.key,
  }) : indicatorSize = APLoaderIndicatorSize.sm;

  final APLoaderIndicatorSize indicatorSize;

  @override
  State<APLoaderIndicator> createState() => _APLoaderIndicatorState();
}

class _APLoaderIndicatorState extends State<APLoaderIndicator> with TickerProviderStateMixin {
  late AnimationController controller;

  @override
  void initState() {
    controller = AnimationController(
      /// [AnimationController]s can be created with `vsync: this` because of
      /// [TickerProviderStateMixin].
      vsync: this,
      duration: const Duration(seconds: 1),
    )..addListener(() {
        setState(() {});
      });
    controller.repeat(reverse: true);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final progressDefaultColors = theme.extension<ProgressDefaultColors>()!;

    final size = widget.indicatorSize == APLoaderIndicatorSize.sm ? APSizing.size2XM : APSizing.sizeXL;

    return Center(
      child: Padding(
        padding: EdgeInsets.all(
          widget.indicatorSize == APLoaderIndicatorSize.sm ? APSpacing.spacing3XS : APSpacing.spacing2XS,
        ),
        child: SizedBox(
          width: size,
          height: size,
          child: CircularProgressIndicator(
            strokeWidth: widget.indicatorSize == APLoaderIndicatorSize.sm ? 2.0 : APSizing.size4XS,
            value: controller.value,
            color: progressDefaultColors.fill,
            backgroundColor: Colors.transparent,
          ),
        ),
      ),
    );
  }
}
