// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$APSongItemModel {
  String get title => throw _privateConstructorUsedError;
  String get subtitle => throw _privateConstructorUsedError;
  bool get selected => throw _privateConstructorUsedError;
  String? get imagePath => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $APSongItemModelCopyWith<APSongItemModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $APSongItemModelCopyWith<$Res> {
  factory $APSongItemModelCopyWith(
          APSongItemModel value, $Res Function(APSongItemModel) then) =
      _$APSongItemModelCopyWithImpl<$Res, APSongItemModel>;
  @useResult
  $Res call({String title, String subtitle, bool selected, String? imagePath});
}

/// @nodoc
class _$APSongItemModelCopyWithImpl<$Res, $Val extends APSongItemModel>
    implements $APSongItemModelCopyWith<$Res> {
  _$APSongItemModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
    Object? subtitle = null,
    Object? selected = null,
    Object? imagePath = freezed,
  }) {
    return _then(_value.copyWith(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      subtitle: null == subtitle
          ? _value.subtitle
          : subtitle // ignore: cast_nullable_to_non_nullable
              as String,
      selected: null == selected
          ? _value.selected
          : selected // ignore: cast_nullable_to_non_nullable
              as bool,
      imagePath: freezed == imagePath
          ? _value.imagePath
          : imagePath // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$APSongItemModelImplCopyWith<$Res>
    implements $APSongItemModelCopyWith<$Res> {
  factory _$$APSongItemModelImplCopyWith(_$APSongItemModelImpl value,
          $Res Function(_$APSongItemModelImpl) then) =
      __$$APSongItemModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String title, String subtitle, bool selected, String? imagePath});
}

/// @nodoc
class __$$APSongItemModelImplCopyWithImpl<$Res>
    extends _$APSongItemModelCopyWithImpl<$Res, _$APSongItemModelImpl>
    implements _$$APSongItemModelImplCopyWith<$Res> {
  __$$APSongItemModelImplCopyWithImpl(
      _$APSongItemModelImpl _value, $Res Function(_$APSongItemModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
    Object? subtitle = null,
    Object? selected = null,
    Object? imagePath = freezed,
  }) {
    return _then(_$APSongItemModelImpl(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      subtitle: null == subtitle
          ? _value.subtitle
          : subtitle // ignore: cast_nullable_to_non_nullable
              as String,
      selected: null == selected
          ? _value.selected
          : selected // ignore: cast_nullable_to_non_nullable
              as bool,
      imagePath: freezed == imagePath
          ? _value.imagePath
          : imagePath // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$APSongItemModelImpl implements _APSongItemModel {
  const _$APSongItemModelImpl(
      {required this.title,
      required this.subtitle,
      required this.selected,
      this.imagePath});

  @override
  final String title;
  @override
  final String subtitle;
  @override
  final bool selected;
  @override
  final String? imagePath;

  @override
  String toString() {
    return 'APSongItemModel(title: $title, subtitle: $subtitle, selected: $selected, imagePath: $imagePath)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$APSongItemModelImpl &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.subtitle, subtitle) ||
                other.subtitle == subtitle) &&
            (identical(other.selected, selected) ||
                other.selected == selected) &&
            (identical(other.imagePath, imagePath) ||
                other.imagePath == imagePath));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, title, subtitle, selected, imagePath);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$APSongItemModelImplCopyWith<_$APSongItemModelImpl> get copyWith =>
      __$$APSongItemModelImplCopyWithImpl<_$APSongItemModelImpl>(
          this, _$identity);
}

abstract class _APSongItemModel implements APSongItemModel {
  const factory _APSongItemModel(
      {required final String title,
      required final String subtitle,
      required final bool selected,
      final String? imagePath}) = _$APSongItemModelImpl;

  @override
  String get title;
  @override
  String get subtitle;
  @override
  bool get selected;
  @override
  String? get imagePath;
  @override
  @JsonKey(ignore: true)
  _$$APSongItemModelImplCopyWith<_$APSongItemModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
