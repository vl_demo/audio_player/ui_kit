import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class APSongItemModel with _$APSongItemModel {
  const factory APSongItemModel({
    required String title,
    required String subtitle,
    required bool selected,
    String? imagePath,
  }) = _APSongItemModel;
}
