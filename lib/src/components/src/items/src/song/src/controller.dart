part of '../index.dart';

class APSongItemController extends ValueNotifier<APSongItemModel> {
  APSongItemController({
    required APSongItemModel model,
  }) : super(
          model,
        );

  set title(String v) => value = value.copyWith(title: v);

  set subtitle(String v) => value = value.copyWith(subtitle: v);

  set selected(bool v) => value = value.copyWith(selected: v);
}
