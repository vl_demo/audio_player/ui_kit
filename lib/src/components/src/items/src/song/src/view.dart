part of '../index.dart';

class APSongItem extends StatelessWidget {
  APSongItem({
    super.key,
    required APSongItemModel model,
  }) : controller = APSongItemController(model: model);

  final APSongItemController controller;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;
    final buttonSecondaryColors = theme.extension<ButtonSecondaryColors>()!;
    final buttonPrimaryColors = theme.extension<ButtonPrimaryColors>()!;
    final dividerSecondaryColors = theme.extension<DividerSecondaryColors>()!;

    return ValueListenableBuilder<APSongItemModel>(
      valueListenable: controller,
      builder: (
        BuildContext context,
        APSongItemModel value,
        Widget? child,
      ) {
        return Padding(
          padding: const EdgeInsets.only(
            top: APSpacing.spacing2XS,
            right: APSpacing.spacing2XS,
            left: APSpacing.spacing2XS,
          ),
          child: DecoratedBox(
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  offset: const Offset(0, 2),
                  color: dividerSecondaryColors.secondary50,
                  blurRadius: 2.0,
                ),
              ],
              color: value.selected ? buttonPrimaryColors.focusedFill : buttonSecondaryColors.defaultFill,
              border: Border.all(
                color: dividerSecondaryColors.secondary50,
              ),
              borderRadius: const BorderRadius.all(
                Radius.circular(
                  APRadiuses.radiusXS,
                ),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(
                APSpacing.spacingXS,
              ),
              child: Row(
                children: [
                  APSongImage(
                    imagePath: value.imagePath,
                  ),
                  const SizedBox(
                    width: APSpacing.spacingXS,
                  ),
                  Expanded(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          value.title,
                          overflow: TextOverflow.ellipsis,
                          style: textTheme.bodyLarge!.copyWith(
                            color: value.selected ? textSecondaryColors.secondary0 : textSecondaryColors.secondary100,
                          ),
                        ),
                        const SizedBox(
                          height: APSpacing.spacing2XS,
                        ),
                        Text(
                          value.subtitle,
                          overflow: TextOverflow.ellipsis,
                          style: textTheme.bodyMedium!.copyWith(
                            color: value.selected ? textSecondaryColors.secondary0 : textSecondaryColors.secondary80,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
