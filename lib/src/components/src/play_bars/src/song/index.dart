import 'package:ap_ui_kit/ap_ui_kit.dart';
import 'package:flutter/material.dart';

export 'src/model.dart';

part 'src/controller.dart';
part 'src/view.dart';
