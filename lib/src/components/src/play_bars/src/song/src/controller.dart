part of '../index.dart';

class APSongPlayBarController extends ValueNotifier<APSongPlayBarModel> {
  APSongPlayBarController({
    required String title,
    required String subtitle,
    required APSongProgressStateType stateType,
    String? imagePath,
    required int secondsTotal,
    required double progress,
  })  : _headerPlayBarController = APSongHeaderPlayBarController(
          model: APSongHeaderPlayBarModel(
            title: title,
            subtitle: subtitle,
            stateType: stateType,
            imagePath: imagePath,
          ),
        ),
        _progressPlayBarController = APSongProgressPlayBarController(
          model: APSongProgressPlayBarModel(
            secondsTotal: secondsTotal,
            progress: progress,
            stateType: stateType,
          ),
        ),
        super(
          const APSongPlayBarModel(
            stateType: APSongProgressStateType.play,
          ),
        );

  APSongPlayBarController.empty()
      : _headerPlayBarController = APSongHeaderPlayBarController.empty(),
        _progressPlayBarController = APSongProgressPlayBarController.empty(),
        super(
          const APSongPlayBarModel(
            stateType: APSongProgressStateType.play,
          ),
        );

  final APSongHeaderPlayBarController _headerPlayBarController;
  final APSongProgressPlayBarController _progressPlayBarController;

  void init(
    APSongHeaderPlayBarModel headerModel,
    APSongProgressPlayBarModel progressModel,
  ) {
    _headerPlayBarController.init(headerModel);
    _progressPlayBarController.init(progressModel);
  }

  set headerData(APSongHeaderPlayBarModel headerModel) => _headerPlayBarController.init(
        headerModel,
      );

  set progressData(APSongProgressPlayBarModel progressModel) => _progressPlayBarController.init(
        progressModel,
      );

  set progress(double v) => _progressPlayBarController.progress = v;

  set stateType(APSongProgressStateType v) => _headerPlayBarController.stateType = v;

  set imagePath(String? v) => _headerPlayBarController.imagePath = v;
}
