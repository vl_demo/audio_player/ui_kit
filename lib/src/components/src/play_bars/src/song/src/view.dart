part of '../index.dart';

class APSongPlayBar extends StatelessWidget {
  const APSongPlayBar({
    super.key,
    required this.controller,
    required this.onPlayTap,
  });

  final APSongPlayBarController controller;
  final void Function(APSongProgressStateType stateType) onPlayTap;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final buttonSecondaryColors = theme.extension<ButtonInvertTertiaryColors>()!;
    final dividerSecondaryColors = theme.extension<DividerSecondaryColors>()!;

    return ValueListenableBuilder<APSongPlayBarModel>(
      valueListenable: controller,
      builder: (
        BuildContext context,
        APSongPlayBarModel value,
        Widget? child,
      ) {
        return DecoratedBox(
          decoration: BoxDecoration(
            color: buttonSecondaryColors.defaultFill,
            border: Border(
              top: BorderSide(
                color: dividerSecondaryColors.secondary50,
              ),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(
              APSpacing.spacingXS,
            ),
            child: Column(
              children: [
                APSongHeaderPlayBar.controller(
                  controller: controller._headerPlayBarController,
                  onPlayTap: onPlayTap,
                ),
                APSongProgressPlayBar.controller(
                  controller: controller._progressPlayBarController,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
