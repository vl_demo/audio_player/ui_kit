import 'package:ap_ui_kit/ap_ui_kit.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class APSongPlayBarModel with _$APSongPlayBarModel {
  const factory APSongPlayBarModel({
    required APSongProgressStateType stateType,
  }) = _APSongPlayBarModel;
}
