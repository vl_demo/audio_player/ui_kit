// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$APSongPlayBarModel {
  APSongProgressStateType get stateType => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $APSongPlayBarModelCopyWith<APSongPlayBarModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $APSongPlayBarModelCopyWith<$Res> {
  factory $APSongPlayBarModelCopyWith(
          APSongPlayBarModel value, $Res Function(APSongPlayBarModel) then) =
      _$APSongPlayBarModelCopyWithImpl<$Res, APSongPlayBarModel>;
  @useResult
  $Res call({APSongProgressStateType stateType});
}

/// @nodoc
class _$APSongPlayBarModelCopyWithImpl<$Res, $Val extends APSongPlayBarModel>
    implements $APSongPlayBarModelCopyWith<$Res> {
  _$APSongPlayBarModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? stateType = null,
  }) {
    return _then(_value.copyWith(
      stateType: null == stateType
          ? _value.stateType
          : stateType // ignore: cast_nullable_to_non_nullable
              as APSongProgressStateType,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$APSongPlayBarModelImplCopyWith<$Res>
    implements $APSongPlayBarModelCopyWith<$Res> {
  factory _$$APSongPlayBarModelImplCopyWith(_$APSongPlayBarModelImpl value,
          $Res Function(_$APSongPlayBarModelImpl) then) =
      __$$APSongPlayBarModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({APSongProgressStateType stateType});
}

/// @nodoc
class __$$APSongPlayBarModelImplCopyWithImpl<$Res>
    extends _$APSongPlayBarModelCopyWithImpl<$Res, _$APSongPlayBarModelImpl>
    implements _$$APSongPlayBarModelImplCopyWith<$Res> {
  __$$APSongPlayBarModelImplCopyWithImpl(_$APSongPlayBarModelImpl _value,
      $Res Function(_$APSongPlayBarModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? stateType = null,
  }) {
    return _then(_$APSongPlayBarModelImpl(
      stateType: null == stateType
          ? _value.stateType
          : stateType // ignore: cast_nullable_to_non_nullable
              as APSongProgressStateType,
    ));
  }
}

/// @nodoc

class _$APSongPlayBarModelImpl implements _APSongPlayBarModel {
  const _$APSongPlayBarModelImpl({required this.stateType});

  @override
  final APSongProgressStateType stateType;

  @override
  String toString() {
    return 'APSongPlayBarModel(stateType: $stateType)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$APSongPlayBarModelImpl &&
            (identical(other.stateType, stateType) ||
                other.stateType == stateType));
  }

  @override
  int get hashCode => Object.hash(runtimeType, stateType);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$APSongPlayBarModelImplCopyWith<_$APSongPlayBarModelImpl> get copyWith =>
      __$$APSongPlayBarModelImplCopyWithImpl<_$APSongPlayBarModelImpl>(
          this, _$identity);
}

abstract class _APSongPlayBarModel implements APSongPlayBarModel {
  const factory _APSongPlayBarModel(
          {required final APSongProgressStateType stateType}) =
      _$APSongPlayBarModelImpl;

  @override
  APSongProgressStateType get stateType;
  @override
  @JsonKey(ignore: true)
  _$$APSongPlayBarModelImplCopyWith<_$APSongPlayBarModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
