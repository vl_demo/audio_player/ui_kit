// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$APSongHeaderPlayBarModel {
  String get title => throw _privateConstructorUsedError;
  String get subtitle => throw _privateConstructorUsedError;
  String? get imagePath => throw _privateConstructorUsedError;
  APSongProgressStateType get stateType => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $APSongHeaderPlayBarModelCopyWith<APSongHeaderPlayBarModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $APSongHeaderPlayBarModelCopyWith<$Res> {
  factory $APSongHeaderPlayBarModelCopyWith(APSongHeaderPlayBarModel value,
          $Res Function(APSongHeaderPlayBarModel) then) =
      _$APSongHeaderPlayBarModelCopyWithImpl<$Res, APSongHeaderPlayBarModel>;
  @useResult
  $Res call(
      {String title,
      String subtitle,
      String? imagePath,
      APSongProgressStateType stateType});
}

/// @nodoc
class _$APSongHeaderPlayBarModelCopyWithImpl<$Res,
        $Val extends APSongHeaderPlayBarModel>
    implements $APSongHeaderPlayBarModelCopyWith<$Res> {
  _$APSongHeaderPlayBarModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
    Object? subtitle = null,
    Object? imagePath = freezed,
    Object? stateType = null,
  }) {
    return _then(_value.copyWith(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      subtitle: null == subtitle
          ? _value.subtitle
          : subtitle // ignore: cast_nullable_to_non_nullable
              as String,
      imagePath: freezed == imagePath
          ? _value.imagePath
          : imagePath // ignore: cast_nullable_to_non_nullable
              as String?,
      stateType: null == stateType
          ? _value.stateType
          : stateType // ignore: cast_nullable_to_non_nullable
              as APSongProgressStateType,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$APSongHeaderPlayBarModelImplCopyWith<$Res>
    implements $APSongHeaderPlayBarModelCopyWith<$Res> {
  factory _$$APSongHeaderPlayBarModelImplCopyWith(
          _$APSongHeaderPlayBarModelImpl value,
          $Res Function(_$APSongHeaderPlayBarModelImpl) then) =
      __$$APSongHeaderPlayBarModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String title,
      String subtitle,
      String? imagePath,
      APSongProgressStateType stateType});
}

/// @nodoc
class __$$APSongHeaderPlayBarModelImplCopyWithImpl<$Res>
    extends _$APSongHeaderPlayBarModelCopyWithImpl<$Res,
        _$APSongHeaderPlayBarModelImpl>
    implements _$$APSongHeaderPlayBarModelImplCopyWith<$Res> {
  __$$APSongHeaderPlayBarModelImplCopyWithImpl(
      _$APSongHeaderPlayBarModelImpl _value,
      $Res Function(_$APSongHeaderPlayBarModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
    Object? subtitle = null,
    Object? imagePath = freezed,
    Object? stateType = null,
  }) {
    return _then(_$APSongHeaderPlayBarModelImpl(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      subtitle: null == subtitle
          ? _value.subtitle
          : subtitle // ignore: cast_nullable_to_non_nullable
              as String,
      imagePath: freezed == imagePath
          ? _value.imagePath
          : imagePath // ignore: cast_nullable_to_non_nullable
              as String?,
      stateType: null == stateType
          ? _value.stateType
          : stateType // ignore: cast_nullable_to_non_nullable
              as APSongProgressStateType,
    ));
  }
}

/// @nodoc

class _$APSongHeaderPlayBarModelImpl implements _APSongHeaderPlayBarModel {
  const _$APSongHeaderPlayBarModelImpl(
      {required this.title,
      required this.subtitle,
      this.imagePath,
      required this.stateType});

  @override
  final String title;
  @override
  final String subtitle;
  @override
  final String? imagePath;
  @override
  final APSongProgressStateType stateType;

  @override
  String toString() {
    return 'APSongHeaderPlayBarModel(title: $title, subtitle: $subtitle, imagePath: $imagePath, stateType: $stateType)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$APSongHeaderPlayBarModelImpl &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.subtitle, subtitle) ||
                other.subtitle == subtitle) &&
            (identical(other.imagePath, imagePath) ||
                other.imagePath == imagePath) &&
            (identical(other.stateType, stateType) ||
                other.stateType == stateType));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, title, subtitle, imagePath, stateType);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$APSongHeaderPlayBarModelImplCopyWith<_$APSongHeaderPlayBarModelImpl>
      get copyWith => __$$APSongHeaderPlayBarModelImplCopyWithImpl<
          _$APSongHeaderPlayBarModelImpl>(this, _$identity);
}

abstract class _APSongHeaderPlayBarModel implements APSongHeaderPlayBarModel {
  const factory _APSongHeaderPlayBarModel(
          {required final String title,
          required final String subtitle,
          final String? imagePath,
          required final APSongProgressStateType stateType}) =
      _$APSongHeaderPlayBarModelImpl;

  @override
  String get title;
  @override
  String get subtitle;
  @override
  String? get imagePath;
  @override
  APSongProgressStateType get stateType;
  @override
  @JsonKey(ignore: true)
  _$$APSongHeaderPlayBarModelImplCopyWith<_$APSongHeaderPlayBarModelImpl>
      get copyWith => throw _privateConstructorUsedError;
}
