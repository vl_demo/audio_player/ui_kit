part of '../index.dart';

class APSongHeaderPlayBarController extends ValueNotifier<APSongHeaderPlayBarModel> {
  APSongHeaderPlayBarController({
    required APSongHeaderPlayBarModel model,
  }) : super(
          model,
        );

  APSongHeaderPlayBarController.empty()
      : super(
          const APSongHeaderPlayBarModel(
            title: '',
            subtitle: '',
            imagePath: null,
            stateType: APSongProgressStateType.play,
          ),
        );

  void init(APSongHeaderPlayBarModel v) => value = v;

  set stateType(APSongProgressStateType v) => value = value.copyWith(stateType: v);

  set imagePath(String? v) => value = value.copyWith(imagePath: v);
}
