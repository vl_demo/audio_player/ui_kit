import 'package:ap_ui_kit/ap_ui_kit.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class APSongHeaderPlayBarModel with _$APSongHeaderPlayBarModel {
  const factory APSongHeaderPlayBarModel({
    required String title,
    required String subtitle,
    String? imagePath,
    required APSongProgressStateType stateType,
  }) = _APSongHeaderPlayBarModel;
}
