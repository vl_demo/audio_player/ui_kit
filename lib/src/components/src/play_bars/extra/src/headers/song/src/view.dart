part of '../index.dart';

class APSongHeaderPlayBar extends StatelessWidget {
  APSongHeaderPlayBar({
    super.key,
    required this.onPlayTap,
    required APSongHeaderPlayBarModel model,
  }) : controller = APSongHeaderPlayBarController(model: model);

  const APSongHeaderPlayBar.controller({
    super.key,
    required this.onPlayTap,
    required this.controller,
  });

  final APSongHeaderPlayBarController controller;
  final void Function(APSongProgressStateType stateType) onPlayTap;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;

    return ValueListenableBuilder<APSongHeaderPlayBarModel>(
      valueListenable: controller,
      builder: (
        BuildContext context,
        APSongHeaderPlayBarModel value,
        Widget? child,
      ) {
        return Row(
          children: [
            APSongImage(
              imagePath: value.imagePath,
            ),
            const SizedBox(
              width: APSpacing.spacingXS,
            ),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    value.title,
                    style: textTheme.bodyLarge!.copyWith(
                      color: textSecondaryColors.secondary100,
                    ),
                  ),
                  const SizedBox(
                    height: APSpacing.spacing2XS,
                  ),
                  Text(
                    value.subtitle,
                    style: textTheme.bodyMedium!.copyWith(
                      color: textSecondaryColors.secondary60,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              width: APSpacing.spacingXS,
            ),
            APPlayButton(
              stateType: value.stateType,
              onPlayTap: onPlayTap,
            )
          ],
        );
      },
    );
  }
}
