part of '../index.dart';

class APSongProgressPlayBarController extends ValueNotifier<APSongProgressPlayBarModel> {
  APSongProgressPlayBarController({
    required APSongProgressPlayBarModel model,
  }) : super(
          model,
        );

  APSongProgressPlayBarController.empty()
      : super(
          const APSongProgressPlayBarModel(
            secondsTotal: 0,
            progress: 0.0,
            stateType: APSongProgressStateType.play,
          ),
        );

  void init(APSongProgressPlayBarModel v) {
    value = value.copyWith(
      secondsTotal: _checkTime(v.secondsTotal),
      progress: _checkProgress(v.progress),
      stateType: v.stateType,
    );
  }

  set progress(double v) {
    value = value.copyWith(
      progress: _checkProgress(v),
    );
  }

  String get progressSecondsLabel {
    return APConvertString.secondsToString(
      (value.secondsTotal * value.progress).toInt(),
    );
  }

  String get totalSecondsLabel {
    return APConvertString.secondsToString(value.secondsTotal);
  }

  double _checkProgress(double v) => v > 1.0
      ? 1.0
      : v < 0.0
          ? 0.0
          : v;

  int _checkTime(int v) {
    return v < 0 ? 0 : v;
  }
}
