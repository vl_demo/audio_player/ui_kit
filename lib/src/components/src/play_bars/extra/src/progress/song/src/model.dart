import 'package:ap_ui_kit/ap_ui_kit.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class APSongProgressPlayBarModel with _$APSongProgressPlayBarModel {
  const factory APSongProgressPlayBarModel({
    required int secondsTotal,
    required double progress,
    required APSongProgressStateType stateType,
  }) = _APSongProgressPlayBarModel;
}
