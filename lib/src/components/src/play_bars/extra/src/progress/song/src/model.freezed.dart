// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$APSongProgressPlayBarModel {
  int get secondsTotal => throw _privateConstructorUsedError;
  double get progress => throw _privateConstructorUsedError;
  APSongProgressStateType get stateType => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $APSongProgressPlayBarModelCopyWith<APSongProgressPlayBarModel>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $APSongProgressPlayBarModelCopyWith<$Res> {
  factory $APSongProgressPlayBarModelCopyWith(APSongProgressPlayBarModel value,
          $Res Function(APSongProgressPlayBarModel) then) =
      _$APSongProgressPlayBarModelCopyWithImpl<$Res,
          APSongProgressPlayBarModel>;
  @useResult
  $Res call(
      {int secondsTotal, double progress, APSongProgressStateType stateType});
}

/// @nodoc
class _$APSongProgressPlayBarModelCopyWithImpl<$Res,
        $Val extends APSongProgressPlayBarModel>
    implements $APSongProgressPlayBarModelCopyWith<$Res> {
  _$APSongProgressPlayBarModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? secondsTotal = null,
    Object? progress = null,
    Object? stateType = null,
  }) {
    return _then(_value.copyWith(
      secondsTotal: null == secondsTotal
          ? _value.secondsTotal
          : secondsTotal // ignore: cast_nullable_to_non_nullable
              as int,
      progress: null == progress
          ? _value.progress
          : progress // ignore: cast_nullable_to_non_nullable
              as double,
      stateType: null == stateType
          ? _value.stateType
          : stateType // ignore: cast_nullable_to_non_nullable
              as APSongProgressStateType,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$APSongProgressPlayBarModelImplCopyWith<$Res>
    implements $APSongProgressPlayBarModelCopyWith<$Res> {
  factory _$$APSongProgressPlayBarModelImplCopyWith(
          _$APSongProgressPlayBarModelImpl value,
          $Res Function(_$APSongProgressPlayBarModelImpl) then) =
      __$$APSongProgressPlayBarModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int secondsTotal, double progress, APSongProgressStateType stateType});
}

/// @nodoc
class __$$APSongProgressPlayBarModelImplCopyWithImpl<$Res>
    extends _$APSongProgressPlayBarModelCopyWithImpl<$Res,
        _$APSongProgressPlayBarModelImpl>
    implements _$$APSongProgressPlayBarModelImplCopyWith<$Res> {
  __$$APSongProgressPlayBarModelImplCopyWithImpl(
      _$APSongProgressPlayBarModelImpl _value,
      $Res Function(_$APSongProgressPlayBarModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? secondsTotal = null,
    Object? progress = null,
    Object? stateType = null,
  }) {
    return _then(_$APSongProgressPlayBarModelImpl(
      secondsTotal: null == secondsTotal
          ? _value.secondsTotal
          : secondsTotal // ignore: cast_nullable_to_non_nullable
              as int,
      progress: null == progress
          ? _value.progress
          : progress // ignore: cast_nullable_to_non_nullable
              as double,
      stateType: null == stateType
          ? _value.stateType
          : stateType // ignore: cast_nullable_to_non_nullable
              as APSongProgressStateType,
    ));
  }
}

/// @nodoc

class _$APSongProgressPlayBarModelImpl implements _APSongProgressPlayBarModel {
  const _$APSongProgressPlayBarModelImpl(
      {required this.secondsTotal,
      required this.progress,
      required this.stateType});

  @override
  final int secondsTotal;
  @override
  final double progress;
  @override
  final APSongProgressStateType stateType;

  @override
  String toString() {
    return 'APSongProgressPlayBarModel(secondsTotal: $secondsTotal, progress: $progress, stateType: $stateType)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$APSongProgressPlayBarModelImpl &&
            (identical(other.secondsTotal, secondsTotal) ||
                other.secondsTotal == secondsTotal) &&
            (identical(other.progress, progress) ||
                other.progress == progress) &&
            (identical(other.stateType, stateType) ||
                other.stateType == stateType));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, secondsTotal, progress, stateType);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$APSongProgressPlayBarModelImplCopyWith<_$APSongProgressPlayBarModelImpl>
      get copyWith => __$$APSongProgressPlayBarModelImplCopyWithImpl<
          _$APSongProgressPlayBarModelImpl>(this, _$identity);
}

abstract class _APSongProgressPlayBarModel
    implements APSongProgressPlayBarModel {
  const factory _APSongProgressPlayBarModel(
          {required final int secondsTotal,
          required final double progress,
          required final APSongProgressStateType stateType}) =
      _$APSongProgressPlayBarModelImpl;

  @override
  int get secondsTotal;
  @override
  double get progress;
  @override
  APSongProgressStateType get stateType;
  @override
  @JsonKey(ignore: true)
  _$$APSongProgressPlayBarModelImplCopyWith<_$APSongProgressPlayBarModelImpl>
      get copyWith => throw _privateConstructorUsedError;
}
