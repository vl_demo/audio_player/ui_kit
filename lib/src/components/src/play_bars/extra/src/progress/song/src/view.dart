part of '../index.dart';

class APSongProgressPlayBar extends StatelessWidget {
  APSongProgressPlayBar({
    super.key,
    required APSongProgressPlayBarModel model,
  }) : controller = APSongProgressPlayBarController(model: model);

  const APSongProgressPlayBar.controller({
    super.key,
    required this.controller,
  });

  final APSongProgressPlayBarController controller;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;
    final progressDefaultColors = theme.extension<ProgressDefaultColors>()!;

    return ValueListenableBuilder<APSongProgressPlayBarModel>(
      valueListenable: controller,
      builder: (
        BuildContext context,
        APSongProgressPlayBarModel value,
        Widget? child,
      ) {
        return Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: APSpacing.spacingXS,
                vertical: APSpacing.spacing2XS,
              ),
              child: Row(
                children: [
                  Text(
                    controller.progressSecondsLabel,
                    style: textTheme.bodyLarge!.copyWith(
                      color: textSecondaryColors.secondary100,
                    ),
                  ),
                  const Spacer(),
                  Text(
                    controller.totalSecondsLabel,
                    style: textTheme.bodyLarge!.copyWith(
                      color: textSecondaryColors.secondary100,
                    ),
                  )
                ],
              ),
            ),
            LinearProgressIndicator(
              color: progressDefaultColors.fill,
              value: value.progress,
            )
          ],
        );
      },
    );
  }
}
