part of '../index.dart';

class APDefaultAppBar extends AAPAppBar {
  APDefaultAppBar({
    this.rightChild,
    this.centerChild,
    this.leftChild,
    super.onBack,
    title = '',
    super.key,
  }) : controller = APDefaultAppBarController(
          model: APDefaultAppBarModel(
            title: title,
          ),
        );

  final APDefaultAppBarController controller;
  final Widget? leftChild;
  final Widget? centerChild;
  final Widget? rightChild;

  @override
  State<APDefaultAppBar> createState() => _APDefaultAppBarState();
}

class _APDefaultAppBarState extends AAPState<APDefaultAppBar> {
  @override
  Widget buildContent(BuildContext context) {
    return ValueListenableBuilder<APDefaultAppBarModel>(
      valueListenable: widget.controller,
      builder: (
        BuildContext context,
        APDefaultAppBarModel value,
        Widget? child,
      ) {
        return Row(
          children: [
            const SizedBox(
              width: APSpacing.spacing3XS,
            ),
            Expanded(
              child: widget.centerChild ??
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      APTitleAppBar(text: widget.controller.value.title),
                    ],
                  ),
            ),
            const SizedBox(
              width: APSpacing.spacing3XS,
            ),
          ],
        );
      },
    );
  }
}
