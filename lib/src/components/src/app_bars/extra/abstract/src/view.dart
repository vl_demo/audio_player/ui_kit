part of '../index.dart';

abstract class AAPAppBar extends StatefulWidget implements PreferredSizeWidget {
  const AAPAppBar({
    super.key,
    this.onBack,
  });

  static const double _kNavBarPersistentHeight = 70.0;

  final GestureTapCallback? onBack;

  @override
  State<AAPAppBar> createState();

  @override
  Size get preferredSize {
    return const Size.fromHeight(_kNavBarPersistentHeight);
  }
}

abstract class AAPState<T extends StatefulWidget> extends State<T> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final surfaceColors = theme.extension<SurfaceColors>()!;
    final dividerSecondaryColors = theme.extension<DividerSecondaryColors>()!;

    final resultConstruction = buildContent(context);

    return DecoratedBox(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: dividerSecondaryColors.secondary30,
          ),
        ),
      ),
      child: SizedBox(
        width: double.infinity,
        child: Column(
          children: [
            DecoratedBox(
              decoration: BoxDecoration(
                color: surfaceColors.white,
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(
                  APSpacing.spacingS,
                  APSpacing.spacingXL,
                  APSpacing.spacingS,
                  APSpacing.spacing2XS,
                ),
                child: resultConstruction,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildContent(BuildContext context);

  Widget subtitle({required String text}) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;

    return Text(
      text,
      style: textTheme.bodySmall?.copyWith(
        color: textSecondaryColors.secondary70,
      ),
    );
  }

  Widget printingSubtitle({required String text}) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;

    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _circle,
        const SizedBox(width: APSpacing.spacing3XS),
        _circle,
        const SizedBox(width: APSpacing.spacing3XS),
        _circle,
        const SizedBox(width: APSpacing.spacing3XS),
        Text(
          text,
          style: textTheme.bodySmall?.copyWith(
            color: textSecondaryColors.secondary70,
            //height: 1.0,
          ),
        ),
      ],
    );
  }

  Widget get _circle {
    final theme = Theme.of(context);
    final progressNeutralColors = theme.extension<ProgressNeutralColors>()!;

    return Padding(
      padding: const EdgeInsets.only(top: APSpacing.spacing3XS),
      child: SizedBox(
        width: APSizing.size4XS,
        height: APSizing.size4XS,
        child: DecoratedBox(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: progressNeutralColors.icon,
          ),
        ),
      ),
    );
  }

  APImageLoad getImage({
    required String path,
    double? width,
    double? height,
    fit = BoxFit.contain,
    imageLoadType = ImageLoadType.asset,
  }) =>
      APImageLoad(
        path: path,
        width: width,
        height: height,
        fit: fit,
        imageLoadType: imageLoadType,
      );

  Widget get defaultAvatar {
    return SvgPicture.asset(
      Assets.avatarDefault,
      width: APSizing.sizeM,
      height: APSizing.sizeM,
    );
  }
}

class APTitleAppBar extends StatelessWidget {
  const APTitleAppBar({
    required this.text,
    super.key,
  });

  final String text;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;

    return Text(
      text,
      overflow: TextOverflow.ellipsis,
      style: textTheme.headlineLarge?.copyWith(
        color: textSecondaryColors.secondary100,
      ),
    );
  }
}

class APMemberCountChatAppBar extends StatelessWidget {
  const APMemberCountChatAppBar({
    required this.text,
    required this.count,
    super.key,
  });

  final String text;
  final int count;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;

    return Text(
      '$count $text',
      style: textTheme.bodySmall?.copyWith(
        color: textSecondaryColors.secondary70,
      ),
    );
  }
}

class APSubTitleChatAppBar extends StatelessWidget {
  const APSubTitleChatAppBar({
    required this.text,
    super.key,
  });

  final String text;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;

    return Text(
      text,
      style: textTheme.bodySmall?.copyWith(
        color: textSecondaryColors.secondary70,
      ),
    );
  }
}
