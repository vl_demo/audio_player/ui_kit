import 'package:ap_ui_kit/generated/assets.dart';
import 'package:ap_ui_kit/src/extra/src/enums.dart';
import 'package:ap_ui_kit/src/extra/src/image_load.dart';
import 'package:ap_ui_kit/src/resources/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

part 'src/view.dart';
