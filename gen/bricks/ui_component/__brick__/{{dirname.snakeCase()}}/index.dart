import 'package:ap_ui_kit/src/resources/index.dart';
import 'package:flutter/material.dart';

import 'src/model.dart';

export 'src/model.dart';

part 'src/controller.dart';
part 'src/view.dart';
