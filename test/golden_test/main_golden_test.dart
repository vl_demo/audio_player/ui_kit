import 'package:ap_ui_kit/ap_ui_kit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:golden_toolkit/golden_toolkit.dart';

const _surfaceWidth = 800.0;

void main() async {
  await loadAppFonts();

  const defaultTheme = 'defaultTheme';

  group(
    'app_bar/',
    () {
      const path = '$defaultTheme/app_bar/';

      testGoldens('app_bar', (tester) async {
        final builder = GoldenBuilder.column()
          ..addScenario(
            'APDefaultAppBar()',
            _GoldenTestWrapperWidget(
              child: APDefaultAppBar(title: 'Audio Player'),
            ),
          );

        await tester.pumpWidgetBuilder(
          SingleChildScrollView(
            child: builder.build(),
          ),
          surfaceSize: const Size(_surfaceWidth, 0.0),
          wrapper: materialAppWrapper(
            theme: APThemes.defaultTheme,
            platform: TargetPlatform.android,
          ),
        );

        await screenMatchesGolden(
          tester,
          '${path}APDefaultAppBar',
          autoHeight: true,
        );
      });
    },
  );

  group(
    'item/',
    () {
      const path = '$defaultTheme/item/';

      testGoldens('item', (tester) async {
        final builder = GoldenBuilder.column()
          ..addScenario(
            'APSongItem(selected: false)',
            _GoldenTestWrapperWidget(
              child: APSongItem(
                model: const APSongItemModel(
                  title: 'Смельчак и Ветер',
                  subtitle: 'Король и Шут',
                  selected: false,
                ),
              ),
            ),
          )
          ..addScenario(
            'APSongItem(selected: true)',
            _GoldenTestWrapperWidget(
              child: APSongItem(
                model: const APSongItemModel(
                  title: 'Смельчак и Ветер',
                  subtitle: 'Король и Шут',
                  selected: true,
                ),
              ),
            ),
          );

        await tester.pumpWidgetBuilder(
          SingleChildScrollView(
            child: builder.build(),
          ),
          surfaceSize: const Size(_surfaceWidth, 0.0),
          wrapper: materialAppWrapper(
            theme: APThemes.defaultTheme,
            platform: TargetPlatform.android,
          ),
        );

        await screenMatchesGolden(
          tester,
          '${path}APSongItem',
          autoHeight: true,
        );
      });
    },
  );

  group(
    'play_bar/',
    () {
      const path = '$defaultTheme/play_bar/';

      testGoldens('play_bar', (tester) async {
        final builder = GoldenBuilder.column()
          ..addScenario(
            'APSongPlayBar(.play)',
            _GoldenTestWrapperWidget(
              child: APSongPlayBar(
                controller: APSongPlayBarController(
                  title: 'Смельчак и Ветер',
                  subtitle: 'Король и Шут',
                  progress: 0.9,
                  secondsTotal: 1020,
                  stateType: APSongProgressStateType.play,
                ),
                onPlayTap: (APSongProgressStateType stateType) {},
              ),
            ),
          )
          ..addScenario(
            'APSongPlayBar(.pause)',
            _GoldenTestWrapperWidget(
              child: APSongPlayBar(
                controller: APSongPlayBarController(
                  title: 'Смельчак и Ветер',
                  subtitle: 'Король и Шут',
                  progress: 0.5,
                  secondsTotal: 1020,
                  stateType: APSongProgressStateType.pause,
                ),
                onPlayTap: (APSongProgressStateType stateType) {},
              ),
            ),
          )
          ..addScenario(
            'APSongPlayBar(.stop)',
            _GoldenTestWrapperWidget(
              child: APSongPlayBar(
                controller: APSongPlayBarController(
                  title: 'Смельчак и Ветер',
                  subtitle: 'Король и Шут',
                  progress: 0.3,
                  secondsTotal: 1020,
                  stateType: APSongProgressStateType.stop,
                ),
                onPlayTap: (APSongProgressStateType stateType) {},
              ),
            ),
          );

        await tester.pumpWidgetBuilder(
          SingleChildScrollView(
            child: builder.build(),
          ),
          surfaceSize: const Size(_surfaceWidth, 0.0),
          wrapper: materialAppWrapper(
            theme: APThemes.defaultTheme,
            platform: TargetPlatform.android,
          ),
        );

        await screenMatchesGolden(
          tester,
          '${path}APSongPlayBar',
          autoHeight: true,
        );
      });
    },
  );
}

class _GoldenTestWrapperWidget extends StatelessWidget {
  const _GoldenTestWrapperWidget({
    required this.child,
  });

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Center(
        child: child,
      ),
    );
  }
}
